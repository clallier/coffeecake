package engine.trigger;

import com.badlogic.gdx.math.Vector2;

import engine.SimulationController;
import engine.entity.Entity;
import game.behavior.collision.FOV;

public abstract class PositionTrigger extends ConditionTrigger{

	public enum Target {ALLY,ENNEMY};
	private FOV fov;
	Target tar;
	private Entity area;
	private Vector2 pos;

	public PositionTrigger(Entity area, Vector2 pos, Target tar) {
		this.area = area;
		this.tar = tar;
		this.pos = pos;
	}

	@Override
	public void init(SimulationController sim)  {
		super.init(sim);
		this.mSimController.registerEntity(area, pos);
		fov = (FOV) area.createBehavior(new FOV());
		fov.setEnnemy(false);
	}

	@Override
	public boolean isVerified() {
		if(tar == Target.ALLY)
			return fov.hasAllied();
		else
			return fov.hasTarget();
	}
	
	public Entity getArea() {
		return area;
	}
	
}
