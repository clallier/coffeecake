package engine.trigger;

import engine.Trigger;

public abstract class ConditionTrigger extends Trigger {
	
	public abstract boolean isVerified();
	
	public void run() {
		if(isVerified() && isScheduled()){
			if(exec())
				cancel();
		}
	}

	public abstract boolean exec();
	
}
