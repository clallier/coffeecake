package engine.trigger;

import engine.SimulationController;
import game.entity.mobs.Kraken;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.TimeUtils;

public class KrakenTrigger extends ConditionTrigger {

	long lastTime = TimeUtils.millis();
	Kraken kraken = null;
	
	@Override
	public void init(SimulationController simController) {
		super.init(simController);

		kraken = new Kraken();
		mSimController.registerEntity(kraken, 0, 0);
		kraken.setPosition(kraken.getAnyWaterTile());
	}
	
	@Override
	public boolean isVerified() {
		long deltaTime = TimeUtils.millis()-lastTime;
		
		if(deltaTime > 2000) { // 4 sec
			// System.out.println("last exec:" + deltaTime);
			return true;
		}
		return false;
	}

	@Override
	public boolean exec() {
		
		lastTime = TimeUtils.millis();
		
		if(kraken.isUnderWater()) {
			kraken.move();	
		} else if(MathUtils.random() > .5f) {
			kraken.move();
		} else {
			kraken.shoot();
		}
		
		return false;
	}

}
