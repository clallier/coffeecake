package engine.trigger;

import engine.entity.CenteredNotification;
import engine.message.PlaySound;
import game.constants.Sounds;
import game.constants.Strings;
import game.entity.Cryotube;
import game.entity.mobs.Kraken;
import game.scene.GameScene;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Predicate;

@SuppressWarnings("unused")
public class KrakenDeathCondition extends ConditionTrigger {

	@Override
	public boolean isVerified() {
		@SuppressWarnings({ "rawtypes", "unchecked" })
		Array<Kraken> array = mSimController.getEntities(new Predicate() {
			@Override
			public boolean evaluate(Object e) {
				if(e instanceof Kraken) return true;
				else return false;
			}
			
		});
		
		int totalPV = 0;
		
		for(Kraken n : array)
			totalPV += n.getDammage().getPV();
		
		if(totalPV > 0)
			return false;
		else return true;
	}

	@Override
	public boolean exec() {
		if(mSimController.getActiveScene() instanceof GameScene) {
			GameScene sc = (GameScene) mSimController.getActiveScene();
			sc.endOflvl();
			mSimController.sendMessage(new PlaySound(Sounds.rocketHit));
			mSimController.registerEntity(new CenteredNotification(Strings.victory), 0, 0);
			System.out.println("EOLvl");
		}
	return true;
	}
}
