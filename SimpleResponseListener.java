package engine;

import com.badlogic.gdx.Net.HttpResponse;
import com.badlogic.gdx.Net.HttpResponseListener;

public class SimpleResponseListener implements HttpResponseListener {
	boolean status = false;
	
	@Override
	public void handleHttpResponse(HttpResponse httpResponse) {
		if(httpResponse.getStatus().getStatusCode() == 200) // all right ! 
			status = true;
	}
	
	@Override
	public void failed(Throwable t) {
		status = false;
	}
	
	protected boolean getStatus(){
		return status;
	}
};

