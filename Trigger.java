package engine;

import com.badlogic.gdx.utils.Timer.Task;
 
public abstract class Trigger extends Task { 
	
	protected SimulationController mSimController;
		
	public void init(SimulationController simController) {
		mSimController = simController;
	}
}
