package engine;

import java.util.HashMap;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.Controllers;

import engine.InputController.InputType;
import engine.defines.EngineValues;
import engine.input.AllKeyListener;
import engine.input.InputManager;
import engine.input.KeyInputProcessor;
import engine.input.PadInputAdapter;
import engine.message.BaseMessage;

public class GameScreen implements Screen {

	protected SimulationController mSimController;
	protected SimulationRenderer mSimRenderer;

	protected final float mUpdateTime = 16.f;

	private InputMultiplexer mInputMultiplexer;
	private InputController mInputs;
	private HashMap<String, InputType> typeMap = new HashMap<String, InputController.InputType>();
	private AllKeyListener mFullListener;
	private InputManager test;

	
	public GameScreen(SimulationController simController, SimulationRenderer simulationRenderer){
		mSimController = simController;
		mSimRenderer = simulationRenderer;
		mSimRenderer.setSimulation(mSimController);
		mSimController.setRenderer(mSimRenderer);
		
		mInputMultiplexer = new InputMultiplexer();
		int i = 1;
		test = new InputManager(this);
		
		mFullListener = new AllKeyListener(this);
		mInputs = new InputController(mFullListener);

		if(Gdx.app.getType() != ApplicationType.Android)
		for(Controller controller: Controllers.getControllers()) {
			addListener(controller,i);
			i++;
		}
		KeyInputProcessor k = new KeyInputProcessor(this, -1, false,false);
		KeyInputProcessor n = new KeyInputProcessor(this, -1, true,false);
		mInputs.addInput(InputType.Keyboard,k);
		mInputs.addInput(InputType.Numpad_and_mouse,n);
		mInputMultiplexer.addProcessor(mFullListener);
		mInputMultiplexer.addProcessor(test);
		mInputMultiplexer.addProcessor(k);
		mInputMultiplexer.addProcessor(n);
		mInputMultiplexer.addProcessor(mSimRenderer.getStage());
		mSimController.setInputs(mInputs);
		mSimController.start("game");
	}

	
	
	public void addListener(Controller controller,int i) {
		PadInputAdapter in = new PadInputAdapter(this,controller);
		switch(i){
		case 1:
			typeMap.put(controller.getName(), InputType.J1);
			mInputs.addInput(InputType.J1, in);
			break;
		case 2 :
			typeMap.put(controller.getName(), InputType.J2);
			mInputs.addInput(InputType.J2, in);
			break;
		case 3 :typeMap.put(controller.getName(), InputType.J3);
			mInputs.addInput(InputType.J3, in);
			break;
		case 4 :
			typeMap.put(controller.getName(), InputType.J4);
			mInputs.addInput(InputType.J4, in);
			break;
		}
		controller.addListener(mFullListener);
		controller.addListener(in);
	}

	@Override
	public void show() {


	}


	@Override
	public void render(float delta) {
		mSimController.update(delta*1000);
		mSimRenderer.render(delta);
	}

	@Override
	public void resize(int width, int height) {
		// recalculate viewport ratio and adapt it
		float r = 1f*width/height;
		float viewportw = EngineValues.viewportW * r;
		float viewporth = EngineValues.viewportH;
		mSimRenderer.setCamViewport((int)viewportw, (int)viewporth);
	}

	@Override
	public void hide() {
		pause();
	}

	@Override
	public void pause() {
		mSimController.pause();
	}

	@Override
	public void resume() {
		mSimController.resume();
	}

	@Override
	public void dispose() {
		mSimController.dispose();
		mSimRenderer.dispose();
	}

	public InputProcessor getInputProcessors() {
		return mInputMultiplexer;
	}

	public void sendMessage(BaseMessage msg) {
		mSimController.sendMessage(msg);
	}

	public SimulationController getSim() {
		return mSimController;
	}

	public InputType getInputType(Controller controller) {
		return typeMap.get(controller.getName());
	}
}
