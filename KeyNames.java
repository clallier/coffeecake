package engine;

import java.util.HashMap;

import engine.InputController.InputType;

public class KeyNames {
	public static HashMap<Integer,String>keyNames = new HashMap<Integer,String>();
	public static final String Key0 = "UNKNOWN";
	public static final String Key1 = "SOFT_LEFT";
	public static final String Key2 = "SOFT_RIGHT";
	public static final String Key3 = "HOME";
	public static final String Key4 = "BACK";
	public static final String Key5 = "CALL";
	public static final String Key6 = "ENDCALL";
	public static final String Key7 = "NUM_0";
	public static final String Key8 = "NUM_1";
	public static final String Key9 = "NUM_2";
	public static final String Key10 = "NUM_3";
	public static final String Key11 = "NUM_4";
	public static final String Key12 = "NUM_5";
	public static final String Key13 = "NUM_6";
	public static final String Key14 = "NUM_7";
	public static final String Key15 = "NUM_8";
	public static final String Key16 = "NUM_9";
	public static final String Key17 = "STAR";
	public static final String Key18 = "POUND";
	public static final String Key19 = "UP";
	public static final String Key20 = "DOWN";
	public static final String Key21 = "LEFT";
	public static final String Key22 = "RIGHT";
	public static final String Key23 = "CENTER";
	public static final String Key24 = "VOLUME_UP";
	public static final String Key25 = "VOLUME_DOWN";
	public static final String Key26 = "POWER";
	public static final String Key27 = "CAMERA";
	public static final String Key28 = "CLEAR";
	public static final String Key29 = "A";
	public static final String Key30 = "B";
	public static final String Key31 = "C";
	public static final String Key32 = "D";
	public static final String Key33 = "E";
	public static final String Key34 = "F";
	public static final String Key35 = "G";
	public static final String Key36 = "H";
	public static final String Key37 = "I";
	public static final String Key38 = "J";
	public static final String Key39 = "K";
	public static final String Key40 = "L";
	public static final String Key41 = "M";
	public static final String Key42 = "N";
	public static final String Key43 = "O";
	public static final String Key44 = "P";
	public static final String Key45 = "Q";
	public static final String Key46 = "R";
	public static final String Key47 = "S";
	public static final String Key48 = "T";
	public static final String Key49 = "U";
	public static final String Key50 = "V";
	public static final String Key51 = "W";
	public static final String Key52 = "X";
	public static final String Key53 = "Y";
	public static final String Key54 = "Z";
	public static final String Key55 = "COMMA";
	public static final String Key56 = "PERIOD";
	public static final String Key57 = "ALT_LEFT";
	public static final String Key58 = "ALT_RIGHT";
	public static final String Key59 = "SHIFT_LEFT";
	public static final String Key60 = "SHIFT_RIGHT";
	public static final String Key61 = "TAB";
	public static final String Key62 = "SPACE";
	public static final String Key63 = "SYM";
	public static final String Key64 = "EXPLORER";
	public static final String Key65 = "ENVELOPE";
	public static final String Key66 = "ENTER";
	public static final String Key67 = "BACKSPACE";
	public static final String Key68 = "GRAVE";
	public static final String Key69 = "MINUS";
	public static final String Key70 = "EQUALS";
	public static final String Key71 = "LEFT_BRACKET";
	public static final String Key72 = "RIGHT_BRACKET";
	public static final String Key73 = "BACKSLASH";
	public static final String Key74 = "SEMICOLON";
	public static final String Key75 = "APOSTROPHE";
	public static final String Key76 = "SLASH";
	public static final String Key77 = "AT";
	public static final String Key78 = "NUM";
	public static final String Key79 = "HEADSETHOOK";
	public static final String Key80 = "FOCUS";
	public static final String Key81 = "PLUS";
	public static final String Key82 = "MENU";
	public static final String Key83 = "NOTIFICATION";
	public static final String Key84 = "SEARCH";
	public static final String Key85 = "MEDIA_PLAY_PAUSE";
	public static final String Key86 = "MEDIA_STOP";
	public static final String Key87 = "MEDIA_NEXT";
	public static final String Key88 = "MEDIA_PREVIOUS";
	public static final String Key89 = "MEDIA_REWIND";
	public static final String Key90 = "MEDIA_FAST_FORWARD";
	public static final String Key91 = "MUTE";
	public static final String Key92 = "PAGE_UP";
	public static final String Key93 = "PAGE_DOWN";
	public static final String Key94 = "PICTSYMBOLS";
	public static final String Key95 = "SWITCH_CHARSET";
	public static final String Key96 = "BUTTON_A";
	public static final String Key97 = "BUTTON_B";
	public static final String Key98 = "BUTTON_C";
	public static final String Key99 = "BUTTON_X";
	public static final String Key100 = "BUTTON_Y";
	public static final String Key101 = "BUTTON_Z";
	public static final String Key102 = "BUTTON_L1";
	public static final String Key103 = "BUTTON_R1";
	public static final String Key104 = "BUTTON_L2";
	public static final String Key105 = "BUTTON_R2";
	public static final String Key106 = "BUTTON_THUMBL";
	public static final String Key107 = "BUTTON_THUMBR";
	public static final String Key108 = "BUTTON_START";
	public static final String Key109 = "BUTTON_SELECT";
	public static final String Key110 = "BUTTON_MODE";
	public static final String Key112 = "FORWARD_DEL";
	public static final String Key129 = "CONTROL_LEFT";
	public static final String Key130 = "CONTROL_RIGHT";
	public static final String Key131 = "ESCAPE";
	public static final String Key132 = "END";
	public static final String Key133 = "INSERT";
	public static final String Key243 = "COLON";
	public static final String Key244 = "F1";
	public static final String Key245 = "F2";
	public static final String Key246 = "F3";
	public static final String Key247 = "F4";
	public static final String Key248 = "F5";
	public static final String Key249 = "F6";
	public static final String Key250 = "F7";
	public static final String Key251 = "F8";
	public static final String Key252 = "F9";
	public static final String Key253 = "F10";
	public static final String Key254 = "F11";

	private static boolean init = false;
	private static void init(){
		init = true;
		keyNames.put(0,Key0);
		keyNames.put(1,Key1);
		keyNames.put(2,Key2);
		keyNames.put(3,Key3);
		keyNames.put(4,Key4);
		keyNames.put(5,Key5);
		keyNames.put(6,Key6);
		keyNames.put(7,Key7);
		keyNames.put(8,Key8);
		keyNames.put(9,Key9);
		keyNames.put(10,Key10);
		keyNames.put(11,Key11);
		keyNames.put(12,Key12);
		keyNames.put(13,Key13);
		keyNames.put(14,Key14);
		keyNames.put(15,Key15);
		keyNames.put(17,Key17);
		keyNames.put(16,Key16);
		keyNames.put(19,Key19);
		keyNames.put(18,Key18);
		keyNames.put(21,Key21);
		keyNames.put(20,Key20);
		keyNames.put(23,Key23);
		keyNames.put(22,Key22);
		keyNames.put(25,Key25);
		keyNames.put(24,Key24);
		keyNames.put(27,Key27);
		keyNames.put(26,Key26);
		keyNames.put(29,Key29);
		keyNames.put(28,Key28);
		keyNames.put(31,Key31);
		keyNames.put(30,Key30);
		keyNames.put(34,Key34);
		keyNames.put(35,Key35);
		keyNames.put(32,Key32);
		keyNames.put(33,Key33);
		keyNames.put(38,Key38);
		keyNames.put(39,Key39);
		keyNames.put(36,Key36);
		keyNames.put(37,Key37);
		keyNames.put(42,Key42);
		keyNames.put(43,Key43);
		keyNames.put(40,Key40);
		keyNames.put(41,Key41);
		keyNames.put(46,Key46);
		keyNames.put(47,Key47);
		keyNames.put(44,Key44);
		keyNames.put(45,Key45);
		keyNames.put(51,Key51);
		keyNames.put(50,Key50);
		keyNames.put(49,Key49);
		keyNames.put(48,Key48);
		keyNames.put(55,Key55);
		keyNames.put(54,Key54);
		keyNames.put(53,Key53);
		keyNames.put(52,Key52);
		keyNames.put(59,Key59);
		keyNames.put(58,Key58);
		keyNames.put(57,Key57);
		keyNames.put(56,Key56);
		keyNames.put(63,Key63);
		keyNames.put(62,Key62);
		keyNames.put(61,Key61);
		keyNames.put(60,Key60);
		keyNames.put(68,Key68);
		keyNames.put(69,Key69);
		keyNames.put(70,Key70);
		keyNames.put(71,Key71);
		keyNames.put(64,Key64);
		keyNames.put(65,Key65);
		keyNames.put(66,Key66);
		keyNames.put(67,Key67);
		keyNames.put(76,Key76);
		keyNames.put(77,Key77);
		keyNames.put(78,Key78);
		keyNames.put(79,Key79);
		keyNames.put(72,Key72);
		keyNames.put(73,Key73);
		keyNames.put(74,Key74);
		keyNames.put(75,Key75);
		keyNames.put(85,Key85);
		keyNames.put(84,Key84);
		keyNames.put(87,Key87);
		keyNames.put(86,Key86);
		keyNames.put(81,Key81);
		keyNames.put(80,Key80);
		keyNames.put(83,Key83);
		keyNames.put(82,Key82);
		keyNames.put(93,Key93);
		keyNames.put(92,Key92);
		keyNames.put(95,Key95);
		keyNames.put(94,Key94);
		keyNames.put(89,Key89);
		keyNames.put(88,Key88);
		keyNames.put(91,Key91);
		keyNames.put(90,Key90);
		keyNames.put(102,Key102);
		keyNames.put(103,Key103);
		keyNames.put(100,Key100);
		keyNames.put(101,Key101);
		keyNames.put(98,Key98);
		keyNames.put(99,Key99);
		keyNames.put(96,Key96);
		keyNames.put(97,Key97);
		keyNames.put(110,Key110);
		keyNames.put(108,Key108);
		keyNames.put(109,Key109);
		keyNames.put(106,Key106);
		keyNames.put(107,Key107);
		keyNames.put(104,Key104);
		keyNames.put(105,Key105);
		keyNames.put(112,Key112);
		keyNames.put(129,Key129);
		keyNames.put(131,Key131);
		keyNames.put(130,Key130);
		keyNames.put(133,Key133);
		keyNames.put(132,Key132);
		keyNames.put(254,Key254);
		keyNames.put(252,Key252);
		keyNames.put(253,Key253);
		keyNames.put(250,Key250);
		keyNames.put(251,Key251);
		keyNames.put(248,Key248);
		keyNames.put(249,Key249);
		keyNames.put(246,Key246);
		keyNames.put(247,Key247);
		keyNames.put(244,Key244);
		keyNames.put(245,Key245);
		keyNames.put(243,Key243);
	}
	public static String getName(InputType t,int key){
		if(!init)
			init();
		if(t == InputType.Keyboard){
			if(!keyNames.containsKey(key))
				return "UNKNOWN";
			return keyNames.get(key);
		}
		if(t == InputType.Numpad_and_mouse){
			if(!keyNames.containsKey(key))
				return "UNKNOWN";
			return keyNames.get(key);
		}
		
		return t.name() + "_B"+key;
	};
}


/**
 * Keep here, used to generate correspondance between keys and string
 */
//int max = 0;
//public void printKeysToFile(String file) throws IOException{ //To be used only on desktop and debug mode to generate a new key correspondance file
//	HashMap<Integer, String> keyNames = new HashMap<Integer, String>();
//	for (Field f : Keys.class.getFields()) {
//		String st =f.getName();
//		try {
//			if(!st.contains("META")){
//				int value = (Integer) f.getInt(null);
//				if(value > max)
//					max = value;
//				keyNames.put(value, st);
//			}
//		} catch (IllegalArgumentException e) {
//			e.printStackTrace();
//		} catch (IllegalAccessException e) {
//			e.printStackTrace();
//		} catch (SecurityException e) {
//			e.printStackTrace();
//		}
//	}
//	FileWriter w = new FileWriter(file);
//	w.write("import java.util.HashMap;\n");
//	w.write("public class KeyNames {\n");
//	w.write("private static HashMap<Integer,String>keyNames = new HashMap<Integer,String>();\n");
//	for (int i = 0 ;i < max; i++) {
//		if(keyNames.containsKey(i)){
//			if(i == -1){
//				w.write("public static final String AnyKey"+" = \"" +keyNames.get(i)+"\";\n");
//			}else{
//				w.write("public static final String Key"+i+" = \"" +keyNames.get(i)+"\";\n");
//			}
//		}
//	}
//	w.write("public void init(){\n");
//	for (Integer i : keyNames.keySet()) {
//		if(i == -1){
//			w.write("keyNames.put("+i+",AnyKey);\n");
//		}
//		else{
//			w.write("keyNames.put("+i+",Key"+i+");\n");
//		}
//	}
//	w.write("}\n");
//
//	w.write("public static String getName(int key){return keyNames.get(key);};\n");
//
//	w.write("}\n");
//	w.close();
//}
