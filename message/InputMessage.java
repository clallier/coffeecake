package engine.message;



import com.badlogic.gdx.math.Vector2;

public class InputMessage extends BaseMessage {
	public Vector2 mXY; // intensity in x/y 
	public Vector2 mDirXY; // intensity in x/y 

	public boolean mAAction; // Pressing "A" action button ?
	public boolean mBAction; // Pressing "B" action button ?
	public boolean used = false; 
	
    public InputMessage( int destinationEntityID, Vector2 xy,Vector2 dir, boolean AAction , boolean BAction)
    {
    	super(destinationEntityID,MessageType.Input);
    	init(xy,dir, AAction, BAction);
    }
    
    protected void init(Vector2 xy,Vector2 dir, boolean AAction, boolean BAction) {
    	mXY = xy;
    	mDirXY = dir;
        mAAction = AAction;
        mBAction = BAction;
    }
}