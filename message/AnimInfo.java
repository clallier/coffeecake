package engine.message;

public class AnimInfo{
	public String state;
	public int count;
	
	public AnimInfo(String state,int count){
		this.state = state;
		this.count=count;
	}
	
	@Override
	public String toString() {
		return state + " " + count;
	}
}