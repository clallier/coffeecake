package engine.message;

import com.badlogic.gdx.math.Vector2;

public class MyInputMessage extends BaseMessage{

	public String action;
	public boolean start = false;
	public Vector2 position = new Vector2();
	public float value = 0;
	
	public MyInputMessage(int id,String action,boolean start) {
		super(id,MessageType.Input);
		this.action = action;
		this.start = start;
	}
	
	public MyInputMessage(int id,String action, float value) {
		super(id,MessageType.Input);
		this.action = action;
		this.value = value;
	}
	
	public MyInputMessage(int id,String action,Vector2 pos) {
		super(id,MessageType.Input);
		this.action = action;
		position.x = pos.x;
		position.y=pos.y;
	}

}
