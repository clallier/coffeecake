package engine.message;

import java.util.ArrayList;

public class ShowTransition extends BaseMessage{

	public String state;
	public int count;
	public ArrayList<AnimInfo> transition;
	
	public ShowTransition(int destinationEntityID, ArrayList<AnimInfo> transition) {
		super(destinationEntityID, MessageType.switchAnim);
		this.transition = transition;
	}

}
