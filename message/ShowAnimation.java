package engine.message;

public class ShowAnimation extends BaseMessage{

	public String state;
	public int count;

	public ShowAnimation(int destinationEntityID, String state,int count) {
		super(destinationEntityID, MessageType.switchAnim);
		this.count = count;
		this.state = state;
	}

}
