package engine.message;


public class PlaySound extends BaseMessage {
	
	public enum Order {play, stop, volume};
	
	public int mEmitter;
	public boolean mIsMusic;
	public String mSource;
	public boolean mIsLoop;
	public Order mOrder;
	public float mVolume;
	
	
	public PlaySound(float volume) {
		super(MessageType.Sound);
		init(null, -1, false, false, Order.volume);
		mVolume = volume;
	}
	
	public PlaySound(String source) {
		super(MessageType.Sound);
		init(source, -1, false, false, Order.play);
	}
	
	public PlaySound(String source, int emitter, boolean isMusic, boolean isLoop, Order order) {
		super(MessageType.Sound);
		init(source, emitter, isMusic, isLoop, order);
	}

	public PlaySound(String source, int emitter, boolean isLoop) {
		super(MessageType.Sound);
		init(source, emitter, false, isLoop, Order.play);
	}

	public PlaySound(String source, int emitter, Order order) {
		super(MessageType.Sound);
		init(source, emitter, false, false, order);
	}

	protected void init(String source, int emitter, boolean isMusic,
			boolean isLoop, Order order) {
		mSource = source;
		mEmitter = emitter;
		mIsMusic = isMusic;
		mIsLoop = isLoop;
		mOrder = order;
	}

	public float getmVolume() {
		return mVolume;
	}

	public void setmVolume(int mVolume) {
		this.mVolume = mVolume;
	}
}
