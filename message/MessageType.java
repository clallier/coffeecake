package engine.message;

public enum MessageType implements IMessageType
{
    Input,
    BeginCollision,
    EndCollision, 
    Delete, 
    Sound, 
    switchAnim, touchpad, touchFire
}
