package engine.scene;

import java.util.ArrayList;

import com.badlogic.gdx.math.Vector2;

import engine.SimulationController;
import engine.entity.Entity;

public abstract class Scene{

	protected SimulationController sim;
	protected ArrayList<Entity> registered = new ArrayList<Entity>();

	
	
	public Scene(SimulationController c){
		this.sim = c;
	}

	private boolean started;
	protected Vector2 pos;

	public void startScene(Vector2 pos){
		this.pos = pos;
		if(!started){
			onStart(pos);
			started = true;
		}else{
			resumeScene();
		}
	}

	private void resumeScene() {
		for (Entity e : registered) {
			sim.toggleEntity(e.getId(), true);
		}
		onResume();
	}

	public void pauseScene() {
		for (Entity e : registered) {
			sim.toggleEntity(e.getId(), false);
		}
		onPause();
	}

	public void stopScene(){
		if(started){
			clear();

			onStop();
			started = false;
		}
	}

	public void clear() {
		for (Entity e : registered) {
			e.destroy();
		}		
	}

	public abstract void onStart(Vector2 pos);

	public abstract void onPause();

	public abstract void onResume();

	public  abstract void onStop();

	public  abstract void onUpdate(float dt);

	public boolean isStarted() {
		return started;
	}

	public void registerEntity(Entity e){
		registered.add(e);
	}


	public SimulationController getSim() {
		return sim;
	}

	public void update(float dt){
		onUpdate(dt);
	}
	
	public Vector2 getPos() {
		return pos;
	}
}
