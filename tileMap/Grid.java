package engine.tileMap;

public interface Grid<T> {

	public T get(int i,int j);
	public int getWidth();
	public int getHeight();
	public void resetData();
	public void add(T data,int i,int j);
	void remove(int line, int col);
}
