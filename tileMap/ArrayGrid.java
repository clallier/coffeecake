package engine.tileMap;

import java.util.Random;



public class ArrayGrid<T extends Object> implements Grid<T>{
	private Object mData[];
	private int mW , mH;

	public ArrayGrid(int w,int h){
		mW = w;
		mH = h;
		resetData();
	}
	
	@Override
	public void resetData() {
		mData = new Object[mW*mH];
	}
		
	@SuppressWarnings("unchecked")
	@Override
	public T get(int line, int col) {
		if(isIn(line, col))
			return (T) mData[getIdx(line,col)];
		else
			return null;
	}
	
	@Override
	public void add(T data, int line, int col) {
		if(isIn(line, col))
			mData[getIdx(line, col)] = data;
	}
	
	@Override
	public void remove(int line, int col) {
		if(isIn(line, col))
			mData[getIdx(line, col)] = null;
	}

	@Override
	public int getWidth() {
		return mW;
	}

	@Override
	public int getHeight() {
		return mH;
	}

	private int getIdx(int line,int col){
		return line*mW+col;
	}

	public boolean isIn(int line ,int col) {
		if(line < 0 || line >= mH || col < 0  || col >= mW)
			return false;
		return true;
	}
	
	public void print(){
		for(int line = 0; line < getHeight();line++){
			for(int col = 0;col < getWidth();col++){
				String str = ""+get(line, col);
				for(int i = str.length(); i < 5;i++){
					str+=" ";
				}
				System.out.print(str);
			}
			System.out.println();
		}
	}

	public static void main(String[] args) {
		Random r = new Random(System.currentTimeMillis());
		int h = r.nextInt(20)+1;
		int w = r.nextInt(20)+1;
		ArrayGrid<Integer> test = new ArrayGrid<Integer>(w, h);
		
		for(int line = 0; line < test.getHeight();line++){
			for(int col = 0;col < test.getWidth();col++){
				test.add(r.nextInt(100), line, col);
			}
		}
		test.print();
	}
	
}
