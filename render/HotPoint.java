package engine.render;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.ArrayMap;

import engine.SimulationRenderer.Layer;
import engine.entity.Entity;
import engine.entity.Entity.Orientation;

public class HotPoint {
	public ArrayMap<Orientation	, Vector2> hotpoints = new ArrayMap<Entity.Orientation, Vector2>();
	protected Layer layer = Layer.Actors;
	
	public HotPoint(Vector2 n, Vector2 s, Vector2 e, Vector2 w) {
		super();
		hotpoints.put(Orientation.N, n);
		hotpoints.put(Orientation.S, s);
		hotpoints.put(Orientation.E, e);
		hotpoints.put(Orientation.W, w);
	}

	public HotPoint(Vector2 n, Vector2 s, Vector2 e, Vector2 w, Layer l) {
		this(n,s,e,w);
		layer = l;
	}

	public HotPoint(HotPoint h) {
		hotpoints.put(Orientation.N, h.getPoint(Orientation.N));
		hotpoints.put(Orientation.S,  h.getPoint(Orientation.S));
		hotpoints.put(Orientation.E,  h.getPoint(Orientation.E));
		hotpoints.put(Orientation.W,  h.getPoint(Orientation.W));	
	}

	public Vector2 getPoint(Orientation o){
		return hotpoints.get(o);
	}
	
	public Layer getLayer() {
		return layer;
	}
}
