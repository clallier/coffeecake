package engine.render;

import java.io.FileNotFoundException;

import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Array;

import engine.Assets;

public class Anim {
	public Array<Drawable> textures = new Array<Drawable>();
	public Array<String> ids = new Array<String>();
	float framerate;
	public Anim(Assets a, Array<String> textures,float framerate) {
		super();
		ids.addAll(textures);
		this.framerate = framerate;
		try {
			init(a);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public Anim(Assets a,String notanimated) {
		super();
		ids.add(notanimated);
		this.framerate = 1;
		try {
			init(a);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public void init(Assets a) throws FileNotFoundException{
		for (String s : ids) {
			textures.add(a.getSkin().getDrawable(s));
		}
	}
	
	public Drawable getTexture(float t){
		if(textures.size == 0)
			return null;
		int i = (int) (t/framerate);
		i%=textures.size;
		return textures.get(i);
	}

	public int getDisplayCount(float dt) {
		return (int) (dt/(framerate*textures.size));
	}

}
