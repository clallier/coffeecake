package engine.render;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class NormalImage extends Image{

	public NormalImage(Drawable drawable,TextureRegion normal) {
		super(drawable);
	}

	public NormalImage() {
		super();
	}

	@Override
	public void draw(SpriteBatch batch, float parentAlpha) {
		validate();

		Color color = getColor();
		batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);

		float x = getX();
		float y = getY();
		float scaleX = getScaleX();
		float scaleY = getScaleY();

		if (getDrawable() != null) {
			if (getDrawable().getClass() == TextureRegionDrawable.class) {
				TextureRegion region = ((TextureRegionDrawable)getDrawable()).getRegion();
				//normal.getTexture().bind(1);
				region.getTexture().bind(0);
				float rotation = getRotation();
				if (scaleX == 1 && scaleY == 1 && rotation == 0)
					batch.draw(region, x + getImageX(), y + getImageY(), getImageWidth(), getImageHeight());
				else {
					batch.draw(region, x + getImageX(), y + getImageY(), getOriginX() - getImageX(), getOriginY() - getImageY(), getImageWidth(), getImageHeight(),
							scaleX, scaleY, rotation);
				}
			} else
				getDrawable().draw(batch, x + getImageX(), y + getImageY(), getImageWidth() * scaleX, getImageHeight() * scaleY);
		}
	}

}
