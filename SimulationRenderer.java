package engine;

import java.util.Comparator;

import box2dLight.RayHandler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;

import engine.defines.EngineValues;
import engine.message.BaseMessage;
import engine.tileMap.ArrayGrid;
import game.behavior.LightMap;
import game.constants.Constants.LightColor;


public class SimulationRenderer implements Disposable {

	public enum Layer{Background, ActorBG, Actors,ActorFG, AbsoluteGUI, Gui, Debug};
	private SimulationController mSimController;
	public Assets mAssets;
	public SoundPlayer mPlayer;
	private SpriteBatch mSpriteBatch,sceneBatch;
	public static RayHandler rayHandler;
	public static ParticleEffect particleEffect;

	private Stage stage;
	protected Array<Table> layers = new Array<Table>();
	Box2DDebugRenderer debugRenderer;
	Matrix4 worldMatrix;

	public SimulationRenderer(Assets assets) {
		mAssets = assets;
		setStage(new Stage());
		sceneBatch = stage.getSpriteBatch();
		for (Layer l : Layer.values()) {
			Table t = new Table();
			layers.insert(l.ordinal(), t);
			stage.addActor(t);
		} 

		layers.get(Layer.AbsoluteGUI.ordinal()).setTransform(true);
		layers.get(Layer.Gui.ordinal()).setTransform(true);
		layers.get(Layer.AbsoluteGUI.ordinal()).setPosition(0, 0);
		setCamViewport(EngineValues.viewportW, EngineValues.viewportH); 
		mSpriteBatch = new SpriteBatch();

		mPlayer = new SoundPlayer(this);

		mSimController = null;

		debugRenderer = new Box2DDebugRenderer();
		debugRenderer.setDrawContacts(true);
		if(EngineValues.awesomeLight)
			initShader();
		
		particleEffect = new ParticleEffect();
		particleEffect.load(Gdx.files.internal("data/particles/particles.p"), getSkin().getAtlas());
	}

	Matrix4 transform = new Matrix4();

	// the ambient intensity (brightness to use when unlit)
	final float DEFAULT_AMBIENT_INTENSITY = LightColor.medium.i();



	// the current ambient intensity
	float ambientIntensity = DEFAULT_AMBIENT_INTENSITY;

	// whether to use attenuation/shadows
	boolean useShadow = true;

	private ShaderProgram program;
	//Texture texture, texture_n;
	private FrameBuffer fbo;
	private Vector2 resolution;
	private Texture pixmaptex;

	// medium light
	public static Vector3 ambientColor = LightColor.medium.v3();

	private void initShader() {
		fbo = new FrameBuffer(Format.RGBA8888, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), false);

		try {
			//read the files into strings
			final String FRAGMENT = Gdx.files.internal("shader/ambiantLight.frag").readString();
			final String VERTEX = Gdx.files.internal("shader/ambiantLight.vert").readString();

			//create our shader program -- be sure to pass SpriteBatch's default attributes!
			program = new ShaderProgram(VERTEX, FRAGMENT);
			// set resolution vector
			resolution = new Vector2(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());

			ShaderProgram.pedantic = false;
			program.begin();
			program.setUniformf("ambientColor", ambientColor.x, ambientColor.y,
					ambientColor.z, ambientIntensity);
			program.setUniformf("resolution", resolution.x,resolution.y);
			program.setUniformi("u_lightmap", 1);
			program.end();
			//Good idea to log any warnings if they exist
			if (program.getLog().length()!=0)
				System.out.println(program.getLog());

			//create our sprite batch
			sceneBatch.setShader(program);
			sceneBatch.setProjectionMatrix(getCam().combined);
			sceneBatch.setTransformMatrix(transform);

		} catch (Exception e) { 
			e.printStackTrace();
		}		
	}


	public void	updateGUIPosition(){
		layers.get(Layer.Gui.ordinal()).setPosition(getCamPos().x,getCamPos().y);
	}

	public void setSimulation(SimulationController simController) {
		mSimController = simController;
		rayHandler = new RayHandler(simController.getWorld());
		//rayHandler.setAmbientLight(ambientColor.x,ambientColor.y,ambientColor.z,ambientIntensity);
	}
	
	int sgn = 1;
	float speed = 0;
	float acc = 0;
	// begin rendering phase
	public void render(float delta) {
		updateGUIPosition();
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		float min = 0.01f;
		float max = 0.5f;
		if(ambientIntensity > max){
			sgn = -1;
			ambientIntensity = max;
		}
		if(ambientIntensity < min){
			sgn = 1;
			ambientIntensity = min;
		}
		speed += sgn*delta*acc;
		ambientIntensity += speed*delta;
		rayHandler.setAmbientLight(ambientIntensity);
		
		worldMatrix=new Matrix4(getCam().combined);
		worldMatrix.scale(EngineValues.bodyScale, EngineValues.bodyScale, 1f);
		rayHandler.setCombinedMatrix(worldMatrix);

		if(EngineValues.awesomeLight){
			computeLight();
		}
		
		sortActors(Layer.Actors);
		sortActors(Layer.ActorFG);
		sortActors(Layer.ActorBG);
		sortActors(Layer.Debug);
		
		getStage().draw();

		sceneBatch.begin();
		particleEffect.update(delta);
		particleEffect.draw(sceneBatch);
		sceneBatch.end();
		if(!EngineValues.awesomeLight)
			rayHandler.updateAndRender();
		if(EngineValues.box2DDebug)
			renderDebug();
	}

	private void sortActors(Layer l) { 
		layers.get(l.ordinal()).getChildren().sort(new Comparator<Actor>() {
			@Override
			public int compare(Actor o1, Actor o2) {
				float y2 = o2.getY();
				float y1 = o1.getY();
				return Double.compare(y2, y1);
			}
		});		
	}


	private void computeLight() {
		fbo.begin();
		sceneBatch.begin();
		Texture t = getLightMap();
		Vector2 p = mSimController.getMap().getAbsolutePosition().cpy().scl(EngineValues.bodyScale).sub(mSimController.getMap().getSize().cpy().scl(EngineValues.bodyScale/2));
		sceneBatch.draw(t,p.x,p.y);
		sceneBatch.end();
		fbo.end();		
		fbo.getColorBufferTexture().bind(1);
	}


	private Texture getLightMap() {
		LightMap light = mSimController.getMap().getLightmap();
		ArrayGrid<Integer> heat = light.getHeatmap();

		Pixmap pixmap = new Pixmap((int)(heat.getWidth()*light.getTilesize()*EngineValues.bodyScale), (int)(heat.getHeight()*light.getTilesize()*EngineValues.bodyScale), Format.RGBA8888 );

		for(int l = 0;l < heat.getHeight();l++){
			for(int c = 0;c < heat.getWidth();c++){
				Integer h = heat.get(l, c);
				if(h == null)
					h = 0;
				pixmap.setColor(new Color(h*1f/light.getMax(),h*1f/light.getMax(),h*1f/light.getMax(),1f));
				Vector2 p = light.getWorldCoord(l,c,light.getHeatmap());
				int x = (int) (p.x*EngineValues.bodyScale);
				int y = (int) (p.y*EngineValues.bodyScale);
				int w = (int) (light.getTilesize()*EngineValues.bodyScale);
				pixmap.fillRectangle(x, y, w, w);
			}
		}
		if(pixmaptex != null)
			pixmaptex.dispose();
		pixmaptex = new Texture( pixmap );
		pixmap.dispose();

		return pixmaptex;
	}


	private void renderDebug() {

		mSpriteBatch.begin();
		debugRenderer.render(mSimController.getWorld(), worldMatrix);
		mSpriteBatch.end();
	}


	public void dispose() {
		mSpriteBatch.dispose();
		debugRenderer.dispose();
		rayHandler.dispose();
	}

	public boolean sendMessage(BaseMessage msg) {
		return mPlayer.sendMessage(msg);
	}

	public void setCamViewport(float width, float height) {
		getStage().getCamera().viewportWidth = width;
		getStage().getCamera().viewportHeight = height; 
		getStage().getCamera().position.set(width *.5f, 0.5f, 0f);
		layers.get(Layer.Gui.ordinal()).setScale(getScale());
		getStage().getCamera().update();
	}

	public Vector2 getCamViewport() {
		return(new Vector2(getStage().getCamera().viewportWidth, getStage().getCamera().viewportHeight));
	}

	public void setCamPos(float x, float y) {
		getStage().getCamera().position.set(x,y,0);
	}

	public Vector3 getCamPos() {
		return(getStage().getCamera().position);
	}

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	public float getScale() {
		float f = getStage().getCamera().viewportWidth;
		return f/Gdx.graphics.getWidth();
	}

	public Camera getCam() {
		return getStage().getCamera();
	}

	/**
	 * @return the skin
	 */
	public Skin getSkin() {
		return mAssets.skin;
	}


	public void setGlobalLight(LightColor lightColor) {
		ambientColor = lightColor.v3();
		ambientIntensity = lightColor.i();
		rayHandler.setAmbientLight(lightColor.color());
	}

	public void setGlobalIntensity(float intensity) {
		ambientIntensity = intensity;
	}
}
