package engine.fsm.transition;

import com.badlogic.gdx.utils.Array;

import engine.fsm.state.State;


public class ComposedTransition extends Transition{

	Array<Transition> transitions = new Array<Transition>();
	
	public ComposedTransition(Array<Transition> transitions) {
		super();
		this.transitions = transitions;
	}

	@Override
	public boolean isVerified() {
		for (Transition t : transitions) {
			if(!t.isVerified()){
				return false;
			}
		}
		return true;
	}

	@Override
	public void exec(State src, State target) {
		for (Transition t : transitions) {
			t.exec(src, target);
		}
	}

}
