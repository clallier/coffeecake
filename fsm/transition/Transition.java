package engine.fsm.transition;

import com.badlogic.gdx.utils.Array;

import engine.fsm.FSM;
import engine.fsm.action.Action;
import engine.fsm.state.State;

public abstract class Transition {
	
	private int id = -1;
	private FSM fsm;
	private Array<Action> actions = new Array<Action>();
	
	public void init(int id,FSM fsm){
		if(this.id == -1){
			this.id = id;
			this.fsm = fsm;
		}
	}

	public void addAction(Action a){
		actions.add(a);
	}
	
	public int getId() {
		return id;
	}

	public FSM getFsm() {
		return fsm;
	}

	public abstract boolean isVerified();

	public void exec(State src,State target){
		for (Action a : actions) {
			a.exec();
		}
	}

	public void update(float dTime) {
		
	}


}
