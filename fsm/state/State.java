package engine.fsm.state;

import engine.SimulationController;
import engine.fsm.FSM;
import engine.message.PlaySound;

public abstract class State {

	private int id = -1;
	private FSM fsm;
	private String execSound = null;
	
	public void setExecSound(String execSound) {
		this.execSound = execSound;
	}
	
	public void init(int id,FSM fsm){
		if(this.id == -1){
			this.id = id;
			this.fsm = fsm;
		}
	}

	public void onEnter(){
		if(execSound != null){
			getSimulation().sendMessage( new PlaySound(execSound));
		}
	}

	public abstract void onExit();

	public abstract void onUpdate(float dTime);

	public int getId() {
		return id;
	}

	public FSM getFsm() {
		return fsm;
	}

	public SimulationController getSimulation(){
		return fsm.getmSim();
	}

}
