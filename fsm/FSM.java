package engine.fsm;
import java.util.ArrayList;
import java.util.HashMap;

import engine.SimulationController;
import engine.fsm.state.State;
import engine.fsm.transition.Transition;

public class FSM{
	private ArrayList<Transition> mConditions;
	private ArrayList<State> mStates;
	private ArrayList<HashMap<Integer,Integer>> mTransitions;

	private int mCurrentState = 0;
	private SimulationController mSim;
	
	public FSM(SimulationController sim){
		mSim = sim;
		mConditions = new ArrayList<Transition>();
		mStates = new ArrayList<State>();
		mTransitions = new ArrayList<HashMap<Integer,Integer>>();
	}

	public void update(float dTime){
		State current =  getCurrentState();
		for (Integer i : mTransitions.get(current.getId()).keySet()) {
			if(mConditions.get(i).isVerified()){
				mCurrentState = mTransitions.get(mCurrentState).get(i);
				current.onExit();
				State p = current;
				current = getCurrentState();
				mConditions.get(i).exec(p, current);
				current.onEnter();
				break;
			}
			mConditions.get(i).update(dTime);
		}
		current.onUpdate(dTime);
	}

	public State createState(State state){
			mStates.add(state);
			state.init(mStates.size()-1,this);
			if(state.getId() == mCurrentState)
				state.onEnter();
			mTransitions.add(state.getId(),new HashMap<Integer, Integer>());
			return state;
	}

	public Transition createTransition(Transition condition, int state,int target){
			mConditions.add(condition);
			condition.init(mConditions.size()-1,this);
			State s = mStates.get(state);
			mTransitions.get(s.getId()).put(condition.getId(),target);
		return condition;
	}
	
	private State getCurrentState() {
		return mStates.get(mCurrentState);
	}
	
	public State getState(int id){
		return mStates.get(id);
	}
	
	public SimulationController getmSim() {
		return mSim;
	}

	public void setmSim(SimulationController mSim) {
		this.mSim = mSim;
	}

}
