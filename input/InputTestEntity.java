package engine.input;

import com.badlogic.gdx.math.Vector2;

import engine.SimulationController;
import engine.entity.Entity;
import engine.message.BaseMessage;

public class InputTestEntity extends Entity{

	private InputManager man;
	
	@Override
	public boolean sendMessage(BaseMessage msg) {
	
		return false;
	}

	public InputTestEntity(InputManager m){
		this.man = m;
	}
	
	@Override
	public void init(int id, SimulationController simController, Vector2 pos) {
		super.init(id, simController, pos);
		man.setListenerId(getId());
	}
	
	@Override
	protected void initFixture() {
		// TODO Auto-generated method stub
	}

}
