package engine.input;

import java.util.HashMap;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerAdapter;
import com.badlogic.gdx.utils.Array;

import engine.GameScreen;
import engine.InputController.InputType;

public class AllKeyListener extends ControllerAdapter implements InputProcessor{


	private GameScreen sc;
	private HashMap<InputType,Array<Integer>> pressed = new HashMap<InputType, Array<Integer>>();
	private HashMap<InputType,Array<Integer>> down = new HashMap<InputType, Array<Integer>>();

	int numKeys[] = {Keys.NUM,Keys.NUM_0,Keys.NUM_1,Keys.NUM_2,Keys.NUM_3,Keys.NUM_4,Keys.NUM_5,Keys.NUM_6,Keys.NUM_7,Keys.NUM_8,Keys.NUM_9,Keys.SLASH,Keys.STAR,Keys.MINUS,Keys.PLUS,Keys.PERIOD};


	public AllKeyListener(GameScreen sc){
		this.sc = sc;
		for(InputType i : InputType.values()){
			pressed.put(i, new Array<Integer>());
			down.put(i, new Array<Integer>());
		}
	}

	public boolean anyKeyDown(InputType t){
		return pressed.get(t).size > 0;
	}

	public void connected(Controller controller){

	}

	public void disconnected(Controller controller){
	}

	public boolean buttonDown(Controller controller, int buttonCode){
		down.get(sc.getInputType(controller)).add(buttonCode);
		pressed.get(sc.getInputType(controller)).add(buttonCode);
		return false;
	}

	public boolean buttonUp (Controller controller, int buttonCode){
		down.get(sc.getInputType(controller)).removeValue(buttonCode, false);
		pressed.get(sc.getInputType(controller)).removeValue(buttonCode,false);
		return false;
	}

	@Override
	public boolean keyDown(int keycode) {
		
		if(isNum(keycode)){
			down.get(InputType.Numpad_and_mouse).add(keycode);
			pressed.get(InputType.Numpad_and_mouse).add(keycode);
		}else{
			down.get(InputType.Keyboard).add(keycode);
			pressed.get(InputType.Keyboard).add(keycode);
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		keyTyped = null;
		if(isNum(keycode)){
			down.get(InputType.Numpad_and_mouse).removeValue(keycode,false);
			pressed.get(InputType.Numpad_and_mouse).removeValue(keycode,false);
		}else{
			down.get(InputType.Keyboard).removeValue(keycode,false);
			pressed.get(InputType.Keyboard).removeValue(keycode,false);
		}
		return false;
	}

	private boolean isNum(int key) {
		for (int k  : numKeys) {
			if(k == key)
				return true;
		}
		return false;
	}
	public Character keyTyped ;
	public Character consumeKey(){
		if(keyTyped != null){
			Character k = new Character(keyTyped);
			keyTyped = null;
			return k;
		}
		else
			return  null;
	}
	@Override
	public boolean keyTyped(char character) {
		keyTyped = character;
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}

	public int getFirstKeyDown(InputType type) {
		Array<Integer> keys = down.get(type);
		if(keys.size == 0)
			return -1;
		int k = keys.get(0);
		keys.clear();
		return k;
	}

}
