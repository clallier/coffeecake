package engine.input;



import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.ObjectMap;

import engine.GameScreen;
import engine.defines.EngineValues;
import engine.entity.Entity;
import engine.message.InputMessage;

// permit to move up, down, left, right the cursor
// permit to valid/cancel an action
public class KeyInputProcessor implements InputProcessor {


	GameScreen screen;
	int mTargetEntityId;
	boolean A = false;
	boolean B = false;
	public enum Action {Up,Down,Left,Right,Fire,Fire2,rotUp,rotDown,rotLeft,rotRight}
	public ObjectMap<Action, Integer> bind = new ObjectMap<Action, Integer>();
	public boolean mouseRot = false;
	private float currentY;
	private float currentX;
	private float currentDirY;
	private float currentDirX;



	public KeyInputProcessor(GameScreen screen,int id,boolean numpad,boolean mouse) {
		this.mTargetEntityId = id;
		this.screen = screen;
		this.mouseRot = mouse;
		if(numpad){
			bind.put(Action.Up,Keys.NUM_8);
			bind.put(Action.Down,Keys.NUM_5);
			bind.put(Action.Left,Keys.NUM_4);
			bind.put(Action.Right,Keys.NUM_6);

			bind.put(Action.rotUp,Keys.Z);
			bind.put(Action.rotDown,Keys.S);
			bind.put(Action.rotLeft,Keys.Q);
			bind.put(Action.rotRight,Keys.D);

			bind.put(Action.Fire,Keys.NUM_7);
			bind.put(Action.Fire2,Keys.NUM_9);

		}else{
			bind.put(Action.Up,Keys.UP);
			bind.put(Action.Down,Keys.DOWN);
			bind.put(Action.Left,Keys.LEFT);
			bind.put(Action.Right,Keys.RIGHT);

			bind.put(Action.rotUp,Keys.Z);
			bind.put(Action.rotDown,Keys.S);
			bind.put(Action.rotLeft,Keys.Q);
			bind.put(Action.rotRight,Keys.D);

			bind.put(Action.Fire,Keys.A);
			bind.put(Action.Fire2,Keys.E);

		}
	}

	public void setBind(ObjectMap<Action, Integer> bind) {
		this.bind = bind;
		for (Action a : Action.values()) {
			if(!bind.containsKey(a)){
				bind.put(a,Keys.UNKNOWN);
			}
		}
	}

	public KeyInputProcessor(GameScreen screen,int id,ObjectMap<Action, Integer> binds) {
		this.mTargetEntityId = id;
		this.screen = screen;
		bind = binds;
	}

	protected void sendMessage() {
		if(currentX > 1)
			currentX = 1;
		if(currentX < -1)
			currentX = -1;

		if(currentY > 1)
			currentY = 1;
		if(currentY < -1)
			currentY = -1;
		screen.sendMessage(new InputMessage(mTargetEntityId, new Vector2(currentX,currentY),new Vector2(currentDirX,currentDirY), A, B));
	}

	@Override
	public boolean touchUp(int screenX,
			int screenY,
			int pointer,
			int button){
		if(mouseRot){
			if(button == Input.Buttons.LEFT)
				A = false;
			if(button == Input.Buttons.RIGHT)
				B = false;
			sendMessage();
			return false;
		}
		
		if(screenX >= EngineValues.screenw/2){
				screen.sendMessage(new  TouchFireMsg(false));
		}
		return false;
	}
	
	@Override
	public boolean touchDragged(int x, int y, int pointeur) {
		if(mouseRot){
			mouseMoved(x, y);
		}
		return false;
	}

	@Override
	public boolean touchDown(int screenX,
			int screenY,
			int pointer,
			int button){
		if(mouseRot){
			if(button == Input.Buttons.LEFT)
				A = true;
			if(button == Input.Buttons.RIGHT)
				B = true;
			sendMessage();
			return false;
		}

		if(screenX < EngineValues.screenw/3){
			screen.sendMessage(new TouchPadMoveMsg(screenX,screenY));
		}else{
			if(screenX >= EngineValues.screenw - EngineValues.screenw/3){
				screen.sendMessage(new  TouchFireMsg(true));
			}		
		}
		return false;
	}

	@Override
	public boolean scrolled(int arg0) {
		if(mTargetEntityId == -1){
			return false;
		}
		return false;
	}

	@Override
	public boolean keyUp(int key) {
		if(mTargetEntityId == -1){
			return false;
		}
		if(key == bind.get(Action.Fire)){
			A = false;
			sendMessage();
		}

		if(key == bind.get(Action.Fire2)){
			B = false;
			sendMessage();
		}

		if (key == bind.get(Action.Left)) {
			currentX += 1;
			sendMessage();
		}

		if (key == bind.get(Action.Right)) {
			currentX -= 1;
			sendMessage();
		}

		if (key == bind.get(Action.Down)) {
			currentY += 1;
			sendMessage();
		}

		if (key == bind.get(Action.Up)) {
			currentY -= 1;
			sendMessage();
		}

		if (key == bind.get(Action.rotLeft)) {
			currentDirX += 1;
			sendMessage();
		}

		if (key == bind.get(Action.rotRight)) {
			currentDirX -= 1;
			sendMessage();
		}

		if (key == bind.get(Action.rotDown)) {
			currentDirY += 1;
			sendMessage();
		}

		if (key == bind.get(Action.rotUp)) {
			currentDirY -= 1;
			sendMessage();
		}

		return false;
	}

	@Override
	public boolean keyTyped(char arg0) {
		return false;
	}

	@Override
	public boolean keyDown(int key) {
		if(mTargetEntityId == -1){
			return false;
		}

		if(key == bind.get(Action.Fire)){
			A = true;
			sendMessage();
		}

		if(key == bind.get(Action.Fire2)){
			B = true;
			sendMessage();	
		}

		if (key == bind.get(Action.Left)) {
			currentX += -1;
			sendMessage();	
		}

		if (key == bind.get(Action.Right)) {
			currentX += 1;
			sendMessage();
		}

		if (key == bind.get(Action.Down)) {
			currentY += -1;
			sendMessage();
		}

		if (key == bind.get(Action.Up)) {
			currentY += 1;
			sendMessage();
		}
		//ROT
		if (key == bind.get(Action.rotLeft)) {
			currentDirX += -1;
			sendMessage();	
		}

		if (key == bind.get(Action.rotRight)) {
			currentDirX += 1;
			sendMessage();
		}

		if (key == bind.get(Action.rotDown)) {
			currentDirY += -1;
			sendMessage();
		}

		if (key == bind.get(Action.rotUp)) {
			currentDirY += 1;
			sendMessage();
		}
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		if(mouseRot && mTargetEntityId > 0){
			Entity e = screen.getSim().getEntity(mTargetEntityId);
			if(e == null)
				return false;
			Vector2 p = e.getAbsolutePosition();
			p.scl(e.getScaleFactor());
			Vector2 mouse = screen.getSim().getSimulationRenderer().getStage().screenToStageCoordinates(new Vector2(screenX,screenY));
			mouse = mouse.sub(p);
			if(Math.abs(mouse.x) > Math.abs(mouse.y)){
				currentDirY = 0;
				if(mouse.x > 0)
					currentDirX = 1;
				else
					currentDirX = -1;
			}else{
				currentDirX = 0;
				if(mouse.y > 0)
					currentDirY = 1;
				else
					currentDirY = -1;
			}
			sendMessage();
		}
		return false;
	}

	public void setId(int id) {
		mTargetEntityId = id;
	}

	public float getCurrentY() {
		return currentY;
	}

	public void setCurrentY(float currentY) {
		this.currentY = currentY;
	}

	public float getCurrentX() {
		return currentX;
	}

	public void setCurrentX(float currentX) {
		this.currentX = currentX;
	}

	public int getmTargetEntityId() {
		return mTargetEntityId;
	}

	public ObjectMap<Action, Integer> getBind() {
		return bind;
	};

	public void setmTargetEntityId(int mTargetEntityId) {
		this.mTargetEntityId = mTargetEntityId;
	}
}
