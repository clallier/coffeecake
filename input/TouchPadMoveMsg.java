package engine.input;

import engine.message.BaseMessage;
import engine.message.MessageType;

public class TouchPadMoveMsg extends BaseMessage {

	public int x,y;
	
	public TouchPadMoveMsg(int x,int y) {
		super(MessageType.touchpad);
		this.x = x;
		this.y = y;
	}

}
