package engine.input;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ObjectMap.Entry;

import engine.GameScreen;
import engine.message.MyInputMessage;

public class InputManager implements InputProcessor {

	protected ObjectMap<Integer, String> mButtonActions;
	protected ObjectMap<Integer, String> mMouseButtonActions;
	protected ObjectMap<Integer, String> mPadButtonActions;
	protected ObjectMap<Integer, String> mAxisActions;

	protected String dragAction;
	protected String moveAction;
	protected GameScreen mScreen;

	int listenerId;

	public InputManager(GameScreen screen){
		mScreen = screen;
		mButtonActions = new ObjectMap<Integer, String>() ;
		mMouseButtonActions = new ObjectMap<Integer, String>();
		mPadButtonActions = new ObjectMap<Integer, String>();
		mAxisActions = new ObjectMap<Integer, String>();
	}

	public void setListenerId(int listenerId) {
		this.listenerId = listenerId;
	}

	public void bindActionToButton(String action,int keycode){
		if(mButtonActions.containsKey(keycode)){
			System.out.println("remapping button " + keycode+ " to " + action);
		}
		mButtonActions.put(keycode, action);
	}

	public void bindActionToMouse(String action,int button){
		if(mMouseButtonActions.containsKey(button)){
			System.out.println("remapping mouse button " + button + " to " + action);
		}
		mMouseButtonActions.put(button, action);
	}

	public void bindActionToMouseMove(String action,boolean moved,boolean dragged){
		if(moved){
			if(moveAction != null){
				System.out.println("remapping mouse move action");
			}
			moveAction = action;
		}
		if(dragged){
			if(dragAction != null){
				System.out.println("remapping mouse dragged action");
			}
			dragAction = action;
		}
	}

	public void bindActionToGamepad(String action,int pad,int button){

	}

	public void bindActionToAxis(String action,int pad,int button){

	}

	@Override
	public boolean keyDown(int keycode) {
		for (Entry<Integer, String> entry : mButtonActions.entries()) {
			if(entry.key == keycode){
				mScreen.sendMessage(new MyInputMessage(listenerId,entry.value, true));
			}
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		for (Entry<Integer, String> entry : mButtonActions.entries()) {
			if(entry.key == keycode){
				mScreen.sendMessage(new MyInputMessage(listenerId,entry.value, false));
			}
		}
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}

}
