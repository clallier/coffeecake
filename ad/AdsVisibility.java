package engine.ad;

public class AdsVisibility {
	public static final int HIDE_ADS = 0;
	public static final int SHOW_BANNER = 1;
	public static final int SHOW_WALL = 2;
}
