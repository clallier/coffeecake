package engine;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.Disposable;

import engine.defines.EngineSounds;
import engine.defines.EngineTextures;
import engine.defines.EngineValues;

public class Assets implements Disposable {

	public AssetManager mAssetManager;
	public final static String atlas = "data/atlas/"+EngineTextures.atlasName+".atlas"; 
	public Skin skin;

	/**
	 * load the atlas by default
	 */
	public Assets() {
		skin = new Skin();
		mAssetManager = new AssetManager();
		
		//load engine stuff
		mAssetManager.load(atlas, TextureAtlas.class);
		mAssetManager.load(EngineValues.font, BitmapFont.class);
		mAssetManager.load(EngineValues.classicFont, BitmapFont.class);
		
		for(String s : EngineSounds.sounds )
			mAssetManager.load(EngineSounds.soundDir+s, Sound.class);
		
	}


	public void loadSounds(String[] sounds) {
		Map<String, Class<?>> toLoad = new HashMap<String, Class<?>>();
		for(String s : sounds)
			toLoad.put(EngineSounds.soundDir+s, Sound.class);
		loadAll(toLoad, true); 
	}

	public void loadMusics(String[] musics) {
		Map<String, Class<?>> toLoad = new HashMap<String, Class<?>>();
		for(String s : musics)
			toLoad.put(EngineSounds.musicDir+s, Music.class);
		loadAll(toLoad, true); 
	}
	
	protected void loadAll(Map<String, Class<?>> toLoad, boolean waitForLoading) {		

		if(toLoad == null)
			return ;

		for(Map.Entry<String, Class<?>> e : toLoad.entrySet() ) {
			mAssetManager.load(e.getKey(), e.getValue());
		}

		if(waitForLoading) {
			mAssetManager.finishLoading();
			TextureAtlas textureAtlas = mAssetManager.get(atlas, TextureAtlas.class); 
			skin = new Skin(textureAtlas);
			
			BitmapFont fnt =  mAssetManager.get(EngineValues.font, BitmapFont.class);
			BitmapFont classicFnt =  mAssetManager.get(EngineValues.font, BitmapFont.class);
			
			skin.add(EngineValues.font, fnt, BitmapFont.class);
			skin.add(EngineValues.classicFont, classicFnt, BitmapFont.class);
			
			LabelStyle ls = new LabelStyle(fnt, Color.WHITE);
			LabelStyle classicLs = new LabelStyle(classicFnt, Color.WHITE);
			skin.add(EngineValues.labelStyle, ls, LabelStyle.class);
			skin.add(EngineValues.classicLabelStyle, classicLs, LabelStyle.class);
			
			NinePatchDrawable up = (NinePatchDrawable) skin.getDrawable(EngineTextures.defaultBtnUp);
			NinePatchDrawable down = (NinePatchDrawable) skin.getDrawable(EngineTextures.defaultBtnDown);

			TextButtonStyle tbs = new TextButtonStyle(up,down,up,fnt);
			TextButtonStyle classicTbs = new TextButtonStyle(up,down,up,classicFnt);			
			skin.add(EngineValues.textButtonStyle, tbs, TextButtonStyle.class);
			skin.add(EngineValues.classicTextButtonStyle, classicTbs, TextButtonStyle.class);

		}
	}

	public float update() {
		float progress = mAssetManager.getProgress();
		if(progress < 1.0f)
			mAssetManager.update();
		return progress;
	}

	/*
	public TextureRegionDrawable getTexture(String fileName) throws FileNotFoundException {
	
		if(!drawables.containsKey(fileName)){
			TextureRegion region = mAtlas.findRegion(fileName); 
			if(region == null){
				throw(new FileNotFoundException(fileName));
			}
			drawables.put(fileName, new TextureRegionDrawable(region));
		}
		return drawables.get(fileName);
	}

	public Sprite getSprite(String spriteName) {
		loadAtlas();
		Sprite sprite = mAtlas.createSprite(spriteName); 
		return sprite;
	}


	public NinePatch getNinePatch(String fileName) {
		loadAtlas();
		NinePatch patch = mAtlas.createPatch(fileName); 
		return patch;
	}

	protected void loadAtlas() {
		if(mAtlas == null) {
			mAtlas = mAssetManager.get(atlas, TextureAtlas.class);
	}
	*/
	/*
	public TextureAtlas getAtlas() {
		return mAtlas;
	}

	public BitmapFont getFont(String fileName) {
		return mAssetManager.get(fileName, BitmapFont.class);
	}
	*/
	
	public Music getMusic(String fileName) {
		return mAssetManager.get(fileName, Music.class);
	}

	public Sound getSound (String fileName) {
		return mAssetManager.get(fileName, Sound.class);
	}

	/**
	 * @return the skin
	 */
	public Skin getSkin() {
		return skin;
	}

	@Override
	public void dispose() {
		mAssetManager.dispose();
		//mAtlas.dispose();
	}
}
