package engine;


import java.util.HashMap;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net.HttpRequest;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ObjectMap.Entry;
import com.badlogic.gdx.utils.Predicate;
import com.badlogic.gdx.utils.Timer;

import engine.InputController.InputType;
import engine.SimulationRenderer.Layer;
import engine.ad.IActivityRequestHandler;
import engine.behavior.ButtonsGUI;
import engine.behavior.LayoutBehavior;
import engine.behavior.TouchpadGUI;
import engine.defines.EngineValues;
import engine.entity.Entity;
import engine.entity.Hero;
import engine.entity.Particle;
import engine.gui.PlayerControlInfo;
import engine.input.InputData;
import engine.message.BaseMessage;
import engine.message.DeletedMessage;
import engine.scene.Scene;
import game.behavior.HeatActor;
import game.entity.SimulationMap;

/**
 * @author clallier
 * abstract class 
 * Its job is to represent a level (a simulation = a level)
 * Should be inited, in order to describe the level.
 * the Update and Render phases are called automatically
 */
public abstract class SimulationController { 
	protected Array<PlayerControlInfo> infos = null;
	protected ILeaderboard board;
	protected boolean launched = false;

	protected static int nextID = 0;
	protected static int nextPID = 0;

	protected String TAG;
	// all scene units
	protected ObjectMap<Integer, Entity> mEntities;
	protected ObjectMap<Integer, Particle> mParticules;

	protected Array<Integer> mSheduledToDeleteEntityKeys;
	protected Array<Integer> mSheduledToDeleteParticleKeys;

	protected Array<Integer> mToToggleOnEntities;
	protected Array<Integer> mToToggleOffEntities;
	protected SimulationRenderer mSimulationRenderer;
	protected IActivityRequestHandler mRequestHandler;
	static SimpleResponseListener listener = new SimpleResponseListener();

	
	
	// map size in tile
	public int TILE_W = -1;
	public int TILE_H = -1;
	// map size in pix
	public float WORLD_W = -1f;
	public float WORLD_H = -1f;
	public static final float TILE_SIZE = 1.6f;

	protected Scene activeScene;
	protected HashMap<String, Scene> scenes = new HashMap<String, Scene>();

	public void addScene(String name, Scene scene){
		scenes.put(name, scene);
	}


	public void switchScene(String name, boolean stop, Vector2 p) {
		if(activeScene != null){
			if(stop)
				activeScene.stopScene();
			else
				activeScene.pauseScene();		}

		activeScene = scenes.get(name);
		activeScene.startScene(p);
	}

	public void switchScene(String name,boolean stop){
		if(activeScene != null){
			if(stop)
				activeScene.stopScene();
			else
				activeScene.pauseScene();		}

		activeScene = scenes.get(name);
		activeScene.startScene(new Vector2());
	}
	//Box 2d World
	protected World mWorld;

	// Timer
	protected Timer mTimer;

	private Array<Entity> players = new Array<Entity>();
	private InputController inputs;
	float dt;
	private SimulationMap map;
	public Array<Entity> getPlayers() {
		return players;
	}

	public InputController getInputs() {
		return inputs;
	}

	public Array<PlayerControlInfo> getInputsInfo() {
		return infos;
	}

	public void setInfos(Array<PlayerControlInfo> infos) {
		this.infos = infos;
	}

	public SimulationController(IActivityRequestHandler requestHandler) {
		TAG = getClass().getName();
		mEntities = new ObjectMap<Integer, Entity>();
		mSheduledToDeleteEntityKeys = new Array<Integer>();
		mToToggleOnEntities = new Array<Integer>();
		mToToggleOffEntities = new Array<Integer>();
		mRequestHandler = requestHandler;
		mParticules = new ObjectMap<Integer, Particle>();
		mSheduledToDeleteParticleKeys = new Array<Integer>();


		mWorld = new World(new Vector2(0, 0), true);

		mTimer = new Timer();		
	}

	public void start(String file) {
		init(file);
	}

	protected void resume() {
	}

	protected void pause() {
	}

	public Entity createPlayer(Entity entity,Vector2 pos,InputData d) {
		if(entity != null) {
			int id = nextID++;
			entity.init(id, this,pos);
			mEntities.put(id, entity);
		} 
		
		players.add(entity);
		inputs.setEntityId(d.type,d.binds,entity.getId(),d.mouseRot);
		getMap().addActor((HeatActor) entity);
		
		if(d.type == InputType.virtualPads) {
			entity.createBehavior(new TouchpadGUI((Hero) entity));
			entity.createBehavior(new ButtonsGUI((Hero) entity));
		}
		
		return entity;
	}

	public Entity registerEntity(Entity entity, float i, float j) {
		if(entity != null) {
			activeScene.registerEntity(entity);
			int id = nextID++;
			entity.init(id, this, new Vector2(i, j));
			mEntities.put(id, entity);
		} 
		return entity;
	}
	
	
	public Entity createEntity(Entity entity, float i, float j) {
		if(entity != null) {
			int id = nextID++;
			entity.init(id, this, new Vector2(i, j));
			mEntities.put(id, entity);
		} 
		return entity;
	}

	/*Create entity as particle, Id are reused , delete message is not sent, particule ids should not be used by app*/
	public Particle createParticle(Particle e,Vector2 pos){
		if(e != null) {
			int pid = nextPID++;
			e.particle = true;
			e.init(pid, this, new Vector2(pos));
			mParticules.put(pid, e);
		} 
		return e;
	}

	public Entity registerEntity(Entity entity, Vector2 p) {
		return this.registerEntity(entity,p.x,p.y);
	}

	/**
	 * 
	 * @param type
	 * @param delay
	 * @param interval 
	 * @param repeatCount : -2 = FOREVER 
	 * @returnj
	 */
	public Trigger createTrigger(Trigger trigger, float delay, float interval, int repeatCount) {
		if(trigger != null) {
			trigger.init(this);
			mTimer.scheduleTask(trigger, delay, interval, repeatCount);
		}
		return trigger;
	}

	/**
	 * Permit to initialize the level on loading
	 * called by the CTor
	 * @param file
	 */
	abstract public void init(String file);

	// Returns true if the entity or any components handled the message
	public boolean sendMessage( BaseMessage msg )
	{
		boolean messageHandled = false;

		// pass message to simulation renderer
		if(getSimulationRenderer() != null)
			messageHandled = getSimulationRenderer().sendMessage(msg);

		// pass message to targeted entity 
		if(!messageHandled && msg.destEntityID != -1) {
			Entity e = mEntities.get(msg.destEntityID);
			if(e != null)
				messageHandled = e.sendMessage(msg);
		}

		// broadcast to all entities
		else if(!messageHandled) //TODO should i broadcst to particule?
			for (Entity e : mEntities.values())
				messageHandled |= e.sendMessage(msg);

		return messageHandled;
	}
	public static long cnt = 0;
	public void update(float dTime) {
		cnt++;
		// update Box2D world

		mWorld.step(dTime/1000f, 1,0);
		dt = dTime;
		// update them all
		for (Entry<Integer, Entity> u : mEntities.entries()) {
			if(u.value == null){
				System.err.println("WTF happened here?");
			}else{
				u.value.update(dTime);
			}
		}

		for (Particle u : mParticules.values()) {
			if(u == null){
				System.err.println("WTF happened here?");
			}else{
				u.update(dTime);
			}
		}
		activeScene.update(dTime);

		// delete entities
		cleanupEntities();
		cleanupParticles();
	}

	public void removeEntity(int id) {
		mSheduledToDeleteEntityKeys.add(id);
	}

	public void removeParticle(int id) {
		mSheduledToDeleteParticleKeys.add(id);
	}

	public void toggleEntity(int id,boolean t) {
		if(t){
			mToToggleOnEntities.add(id);
		}else{
			mToToggleOffEntities.add(id);
		}
	}

	public Entity getEntity(int id){
		return mEntities.get(id);
	}
 
    @SuppressWarnings({"unchecked"})
	public <T> Array<T> getEntities(Predicate<T> filter) {
        Array<T> list = new Array<T>();
    	
    	for (Entity e : mEntities.values()) {
            if (filter.evaluate((T) e)) {
                list.add((T) e);
            }
        }
    	return list;
    }

	public void setInputs(InputController mInputs) {
		this.inputs = mInputs;
	}

	public void dispose() {
		clearScreen();
		mEntities.clear();
		mSheduledToDeleteEntityKeys.clear();
		mSheduledToDeleteParticleKeys.clear();
		mToToggleOnEntities.clear();
		mToToggleOffEntities.clear();
	}


	public void clearScreen() {
		for(int k :mEntities.keys())
			removeEntity(k);
		for(int k :mParticules.keys())
			removeParticle(k);
		players.clear();
		cleanupEntities();
		cleanupParticles();
	}

	private void cleanupEntities() {
		for(Integer i : mSheduledToDeleteEntityKeys ) {
			Entity e = null;
			Body b = null;
			e = mEntities.get(i);
			// delete body 
			if(e != null){
				e.setmDead(true);
				e.dispose();

				sendMessage(new DeletedMessage(i));

				b = e.getBody();
				if(b != null)
					mWorld.destroyBody(e.getBody());

				// delete entity
			}
			mEntities.remove(i);
		}
		for (Integer i : mToToggleOnEntities) {
			mEntities.get(i).getBody().setActive(true);
		}

		for (Integer i : mToToggleOffEntities) {
			mEntities.get(i).getBody().setActive(false);
		}
		mToToggleOffEntities.clear();
		mToToggleOnEntities.clear();
		mSheduledToDeleteEntityKeys.clear();
	}

	private void cleanupParticles() {
		for(Integer i : mSheduledToDeleteParticleKeys ) {
			Particle e = null;
			Body b = null;
			e = mParticules.get(i);
			// delete body 
			if(e != null){
				e.setmDead(true);
				e.dispose();
				b = e.getBody();
				if(b != null)
					mWorld.destroyBody(b);
			}
			mParticules.remove(i);
		}
		mSheduledToDeleteParticleKeys.clear();

	}

	public void setRenderer(SimulationRenderer simRenderer) {
		mSimulationRenderer = simRenderer;
	}

	public void resize(int width, int height) {

	}

	public World getWorld() {
		return mWorld;
	}
	
	public SimulationMap getMap() {
		return map;
	}

	public Assets getAssets(){
		return mSimulationRenderer.mAssets;
	}

	public SimulationRenderer getSimulationRenderer() {
		return mSimulationRenderer;
	}

	//	protected void clearGameScreen() {
	//		getPlayers().clear();
	//		for (Entity e : mEntities.values()) {
	//			e.destroy();
	//		}
	//		for (Entity e : mParticules.values()) {
	//			e.destroy();
	//		}
	//	}

	public ILeaderboard getBoard() {
		return board;
	}

	public void setBoard(ILeaderboard board) {
		this.board = board;
	}

	public static boolean isMobileDevice(){
		return Gdx.app.getType() == ApplicationType.Android || Gdx.app.getType() == ApplicationType.iOS || EngineValues.mobileDebug;
	}

	public static void initInternetTest(){
		HttpRequest request = new HttpRequest("HEAD");
		request.setUrl("http://www.google.com");
		request.setTimeOut(2000);
		Gdx.net.sendHttpRequest(request, listener);
	}

	public static boolean getInternetTest() {
		return listener.getStatus();
	}

	public void showAds(boolean isBanner) {
		mRequestHandler.show(isBanner);
	}

	public void hideAds() {
		mRequestHandler.hide();
	}

	public String leaderBoardName() {
		return board.leaderboardName();
	}

	public void addObstacle(Entity e) {
	}

	public void setWorld(SimulationMap map) {
		this.map = map;
		if(getPlayers().size > 0)
			map.getHeatMap().addTarget(getPlayers().get(0));
		this.TILE_W = (int) map.w;
		this.TILE_H = (int) map.h;
		this.WORLD_W = TILE_W * 1.6f;
		this.WORLD_H = TILE_H * 1.6f;
	}
	
	public SimulationMap getSimulationMap() {
		return this.map;
	}

	public Scene getActiveScene() {
		return activeScene;
	}

	public void addActorToCurrentScene(Actor a, Layer layer) {
		mSimulationRenderer.layers.get(layer.ordinal()).addActor(a);
	}
	
	public void addGUIWidget(LayoutBehavior b){
		if(b.relative){
			addActorToCurrentScene(b.layout, Layer.Gui);
			b.layout.setTransform(true);
		}else{
			addActorToCurrentScene(b.layout, Layer.Actors);
		}
	}


	public void removeObstacle(Entity e) {
		
	}

}
