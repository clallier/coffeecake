package engine.gui;

import engine.behavior.tools.VolumeBehavior;
import engine.entity.Entity;


public class VolumeWidget extends Entity{

	@Override
	protected void initFixture() {
		
	}
	
	@Override
	protected void initActions() {
		super.initActions();
		createBehavior(new VolumeBehavior());
	}
}
