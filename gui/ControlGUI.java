package engine.gui;

import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;

import engine.InputController.InputType;
import engine.SimulationController;
import engine.behavior.LayoutBehavior;
import engine.defines.EngineSounds;
import engine.defines.EngineValues;
import engine.defines.EngineValues.lang;
import engine.entity.Entity;
import engine.input.AllKeyListener;
import engine.input.InputData;
import engine.input.KeyInputProcessor.Action;
import engine.message.PlaySound;
import game.constants.Constants;

public class ControlGUI extends LayoutBehavior{
	int nbPlayer = 0;
	int currentSlot = 0;
	AllKeyListener in;
	Float time = 1f;
	Array<Float> timer = new Array<Float>();
	Array<InputData> inputs = new Array<InputData>();
	boolean slots[];
	boolean taken[];

	Label version ; 
	PlayerControlInfo numpadInfo;
	PlayerControlInfo keyboardInfo;
	Array<PlayerControlInfo> infos = new Array<PlayerControlInfo>();

	private int max;
	private TextButton readyButton;
	
	public ControlGUI(){
		if(Gdx.app.getType() == ApplicationType.WebGL){
			max = 2;
		}else{
			max = 2;
		}
		taken = new boolean[InputType.values().length];

		slots = new boolean[max];
		for(int i = 0; i < slots.length;i++){
			slots[i] = false;
		}
		for(int i = 0; i < taken.length;i++){
			taken[i] = false;
		}
	}

	@Override
	public void init(Entity entity, SimulationController simController) {
		super.init(entity, simController);
		in = mSimController.getInputs().getGlobal();
		for (InputType t : InputType.values()) {
			timer.insert(t.ordinal(), time);
		}
	}

	@Override
	public void initGUI() {
		
		Skin skin = mSimController.getSimulationRenderer().getSkin();
		
		 readyButton = new TextButton("Start", skin, EngineValues.classicTextButtonStyle);
		 readyButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				mEntity.destroy();
				mSimController.setInfos(infos);
				mSimController.switchScene("Scenario", true,new Vector2(30,30));
			}
		});
		
		version = new Label("", skin, EngineValues.labelStyle);
	
		layout.row();

		for(int i = 0;i < slots.length;i++){
			PlayerControlInfo info = new PlayerControlInfo(null,this, mSimController);
			infos.add(info);
			layout.add(info).width(EngineValues.screenw/2).height(EngineValues.screenh/(slots.length+1)).colspan(4).padBottom(40);
			layout.row();
		}
		infos.get(0).setActive(0);
		layout.add(readyButton).colspan(4).width(100).height(50).padTop(20);
		readyButton.setVisible(false);
		layout.setPosition(0,0);
	}

	public void setVersion(String v) {
		version.setText("v"+v);
	}
	
	@Override
	protected void updateGUI(float dt) {
		checkNewPlayer(dt);
		startCheck();
		setBind();
		for (PlayerControlInfo inf : infos) {
			inf.updateGUI(dt);
		}
	}

	private void setBind() {
		for (PlayerControlInfo inf : infos) {
			if(inf.isRebind()){
				int k = in.getFirstKeyDown(inf.getData().type);
				inf.bind(k);
			}
		}
	}

	private void startCheck() {
		if(nbPlayer == 0)
			return;
		readyButton.setVisible(true);
		for (PlayerControlInfo info : infos) {
			if(info.data != null && !info.isReady()){
				return;
			}
		}
		mEntity.destroy();
		mSimController.switchScene("Scenario", true);
	}


	private void checkNewPlayer(float dt) {
		for (InputType t : InputType.values()) {
			if(!taken[t.ordinal()] && in.anyKeyDown(t)){
				Float time = timer.get(t.ordinal());
				time -= dt;
				timer.removeIndex(t.ordinal());
				timer.insert(t.ordinal(), time);
			}else{
				timer.removeIndex(t.ordinal());
				timer.insert(t.ordinal(), time);
			}
		}
		for (InputType t : InputType.values()) {
			Float time = timer.get(t.ordinal());
			if(time <= 0){
				newPlayer(t);
			}
		}
	}

	private void newPlayer(InputType t) {
		if(nbPlayer < slots.length){
			mSimController.sendMessage(new PlaySound(EngineSounds.newPlayer));
			currentSlot = getCurrentSlot();
			taken[t.ordinal()] = true;
			slots[currentSlot] = true;
			inputs.insert(currentSlot,new InputData(currentSlot,t,getBind(t)));
			infos.get(currentSlot).setData(inputs.get(currentSlot),currentSlot);
			nbPlayer++;
			Constants.nbPlayer = nbPlayer;
			if(t == InputType.Keyboard){
				keyboardInfo = infos.get(currentSlot);
				if(numpadInfo != null){
					keyboardInfo.deleteMouseOption();
				}
			}
			if(t == InputType.Numpad_and_mouse){
				numpadInfo = infos.get(currentSlot);
				if(keyboardInfo != null)
					keyboardInfo.deleteMouseOption();
			}
			if(nbPlayer < slots.length)
				infos.get(nbPlayer).setActive(nbPlayer);

		}
	}

	public static ObjectMap<Action, Integer> getBind(InputType t) {
		ObjectMap<Action, Integer> binds = null;
		binds = getBindPref(t);
		if(binds == null){
			binds = new ObjectMap<Action, Integer>();
			switch(t){
			case J1:
				binds.put(Action.Fire,0);
				binds.put(Action.Fire2,1);

				break;
			case J2:
				binds.put(Action.Fire,0);
				binds.put(Action.Fire2,1);

				break;
			case J3:
				binds.put(Action.Fire,0);
				binds.put(Action.Fire2,1);
				break;
			case J4:
				binds.put(Action.Fire,0);
				binds.put(Action.Fire2,1);
				break;
			case Keyboard:
				binds.put(Action.rotUp,Keys.I);
				binds.put(Action.rotDown,Keys.K);
				binds.put(Action.rotLeft,Keys.J);
				binds.put(Action.rotRight,Keys.L);
				if(EngineValues.language == lang.fr){
					binds.put(Action.Up,Keys.Z);
					binds.put(Action.Down,Keys.S);
					binds.put(Action.Left,Keys.Q);
					binds.put(Action.Right,Keys.D);
				}else{
					binds.put(Action.Up,Keys.W);
					binds.put(Action.Down,Keys.S);
					binds.put(Action.Left,Keys.A);
					binds.put(Action.Right,Keys.D);
				}
				binds.put(Action.Fire,Keys.SPACE);
				binds.put(Action.Fire2,Keys.E);

				break;
			case Numpad_and_mouse:
				binds.put(Action.Up,Keys.NUM_8);
				binds.put(Action.Down,Keys.NUM_5);
				binds.put(Action.Left,Keys.NUM_4);
				binds.put(Action.Right,Keys.NUM_6);
				binds.put(Action.Fire2,Keys.NUM_7);
				break;
			default:
				break;
			}
		}	
		return binds;
	}

	private static ObjectMap<Action, Integer> getBindPref(InputType t) {
		return null; //TODO Save user pref
	}

	private int getCurrentSlot() {
		for(int i = 0;i < slots.length;i++){
			if(!slots[i])
				return i;
		}
		return -1;
	}

	public boolean hasNumpad() {
		return numpadInfo != null;
	}

	public boolean hasKeyboard() {
		return keyboardInfo != null;
	}
}
