package engine.gui;


import com.badlogic.gdx.utils.Array;

import engine.behavior.LayoutBehavior;

public abstract class HeroesGUI extends LayoutBehavior {
	protected Array<HeroInfo> infos = new Array<HeroInfo>();


	@Override
	protected void updateGUI(float dt) {
		for (HeroInfo i : infos) {
			i.update(dt);
		}
	}

}
