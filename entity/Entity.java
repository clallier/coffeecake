package engine.entity;


import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Array.ArrayIterator;
import com.badlogic.gdx.utils.Disposable;

import engine.SimulationController;
import engine.behavior.Behavior;
import engine.behavior.ImageBehavior;
import engine.message.BaseMessage;

public abstract class Entity implements Disposable{
	public enum Orientation {N,S,E,W};
	protected float mScaleFactor;
	protected int mId;	//id of the entity 
	public int BEHAVIOR_IDS = 0;

	protected Array<Behavior> mBehaviors;
	protected ImageBehavior mRenderer;
	protected SimulationController mSimController;

	protected Body mBody;
	protected Vector2 mSize = new Vector2();
	protected Orientation orient = Orientation.E;

	private boolean mDestroyed = false;
	public boolean particle = false;
	private boolean obstacle;

	protected Entity()
	{
		mBody = null;
		mBehaviors = new Array<Behavior>();
		mScaleFactor = 10;
	}

	protected PolygonShape createFov(float d,float a) {
		PolygonShape s = new PolygonShape();
		Vector2 o = new Vector2(0,0);
		Vector2 v1 = new Vector2(d*MathUtils.cos(a),d*MathUtils.sin(a));
		Vector2 v2 = new Vector2(v1.x,-v1.y);
		Vector2 array[] = {o,v2,v1};
		s.set(array);
		return s;
	}

	public void setOrient(Orientation orient) {
		this.orient = orient;
	}

	public Orientation getOrient() {
		return orient;
	}

	public Vector2 getSize() {
		return mSize;
	}


	public void setSize(Vector2 size) {
		mSize = size;
	}
	public void setSize(float w,float h) {
		this.mSize = new Vector2(w,h);
	}

	/*A way to know if it has the behavior*/
	public boolean  hasBehaviour(Class<? extends Behavior>c){
		for (Behavior t : mBehaviors) {
			if(t.getClass() == c)
				return true;
		}
		return false;
	}

	public Behavior getBehaviours(Class<? extends Behavior>c){
		for (Behavior t : mBehaviors) {
			if(t.getClass().isAssignableFrom(c))
				return t;
		}
		return null;
	}

	public void removeBehavior(Class<? extends Behavior>c){
		ArrayIterator<Behavior> it = new ArrayIterator<Behavior>(mBehaviors);
		while (it.hasNext()) {
			Behavior b = it.next();
			if( b.getClass().isAssignableFrom(c) ){
				b.dispose();
				it.remove();
			}
		}
	}

	public void removeBehavior(Behavior b) {
		ArrayIterator<Behavior> it = new ArrayIterator<Behavior>(mBehaviors);
		b.setId(-1);
		while (it.hasNext()) {
			Behavior be = it.next();
			if(be.getId() == b.getId()){
				it.remove();
			}
		}
		b.dispose();
	}

	public void init(int id, SimulationController simController, Vector2 pos) {
		this.mId = id;
		mSimController = simController;
		// Body
		initBody(pos);
		//init size of graphic
		mSize = initSize();
		// Fixture
		initFixture();
		// actions 
		initActions();

	}

	protected void initBody(Vector2 pos) {
		BodyDef bodyDef = new BodyDef();
		bodyDef.type = BodyType.DynamicBody;
		bodyDef.position.set(pos);
		mBody = mSimController.getWorld().createBody(bodyDef);
		mBody.setUserData(this);
		setPosition(pos);
	}

	protected Vector2 initSize() {
		return new Vector2(0,0);
	}

	abstract protected void initFixture();

	protected void initActions() {}

	public void update(float dTime) {

		ArrayIterator<Behavior> it = new ArrayIterator<Behavior>(mBehaviors);
		while (it.hasNext()) {
			Behavior b = it.next();
			if(b != null && b.enabled) {
				b.update(dTime);
			}
		}
	}

	/*
	 * Entity has a switch for any messages it cares about
	 * If the entity didn't handle the message but the component
	 * did, we return true to signify it was handled by something.
	 */
	public boolean sendMessage( BaseMessage msg )
	{
		boolean messageHandled = false;
		messageHandled |= passMessageToBehaviors(msg);
		return messageHandled;
	}

	private boolean passMessageToBehaviors( BaseMessage msg )
	{
		boolean messageHandled = false;
		ArrayIterator<Behavior> it = new ArrayIterator<Behavior>(mBehaviors);
		while (it.hasNext()) {
			Behavior b = it.next();
			messageHandled |= b.handleMessage(msg);
		}
		return messageHandled;
	}

	public Behavior createBehavior(Behavior behavior) {
		if(behavior != null) {
			behavior.setId(BEHAVIOR_IDS);
			behavior.init(this, mSimController);
			mBehaviors.add(behavior);
			BEHAVIOR_IDS++;
		}
		return behavior;
	}

	public void updateOrient(float angle) {
		Vector2 v = new Vector2(MathUtils.cos(angle),MathUtils.sin(angle));
		if(Math.abs(v.x) > Math.abs(v.y)){
			if(v.x >= 0 )
				setOrient(Orientation.E);
			if(v.x < 0)
				setOrient(Orientation.W);
		}else{
			if(v.y > 0)
				setOrient(Orientation.N);
			if(v.y < 0)
				setOrient(Orientation.S);
		}
	}

	public void lookAt(Vector2 pos) {
		Vector2 me = getRelativePosition();
		float a = (MathUtils.atan2(-me.y+pos.y, -me.x+pos.x));
		updateOrient(a);
	}

	public float getAngle(){
		switch(orient){
		case N:
			return MathUtils.PI/2;
		case S:
			return 3*MathUtils.PI/2;
		case E:
			return 0;
		case W:
			return MathUtils.PI;
		default:
			return 0;
		}
	}

	public void destroy() {
		if(particle){
			mSimController.removeParticle(getId());
		}else{
			mSimController.removeEntity(getId());
			if(isObstacle()){
				mSimController.removeObstacle(this);
			}
		}
	}
	@Override
	public void dispose() {
		for (Behavior b : mBehaviors) {
			b.dispose();
		}
	}

	public boolean isDead() {
		return mDestroyed ;
	}

	public void setmDead(boolean mDead) {
		this.mDestroyed = mDead;
	}


	public SimulationController getSimulation(){
		return mSimController;
	}


	public float getScaleFactor() {
		return mScaleFactor;
	}


	public ImageBehavior getRenderer() throws NullPointerException {
		return mRenderer;
	}

	public ImageBehavior setRenderer(ImageBehavior renderer) {
		mRenderer = (ImageBehavior)createBehavior(renderer);
		return renderer;
	}


	public int getId() {
		return mId;
	}

	public Body getBody()  {
		return mBody;
	}


	public Vector2 getmSize() {
		return mSize;
	}

	public void setObstacle(boolean obstacle) {
		this.obstacle = obstacle;
	}

	public boolean isObstacle() {
		return obstacle;
	}


	public Vector2 getBodyCenter() {
		return getRelativePosition();
	}

	public void setPosition(Vector2 pos){
		setLocalTransform(pos,getBody().getAngle());
	}

	public Vector2 getAbsolutePosition() {
		return getBody().getPosition().cpy();
	}

	public Vector2 getRelativePosition() {
		return getAbsolutePosition().sub(getAnchorPos());
	}

	protected Vector2 getAnchorPos() {
		return mSimController.getActiveScene().getPos();
	}


	public void setLocalTransform(Vector2 pos, float angle) {
		getBody().setTransform(pos.add(getAnchorPos()), angle);
	}


	public int getLocalZIndex() {
		int y = (int)(getRelativePosition().y+mSize.y);
		if (y < 0)
			y = 0;
		return y;
	}
}
