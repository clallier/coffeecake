package engine.entity;

import com.badlogic.gdx.math.Vector2;

public abstract class Hero extends Entity {

	public abstract void setDir(Vector2 d);
	public abstract void setAB(boolean pressed, boolean pressed2);
	public abstract void aimAt(Vector2 d);

}
