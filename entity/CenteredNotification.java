package engine.entity;

import com.badlogic.gdx.math.Vector2;

import engine.SimulationController;


public class CenteredNotification extends Notification {

	public CenteredNotification(String s) {
		super(s, true);
	}
	
	@Override
	public void init(int id, SimulationController simController, Vector2 pos) {
		
		Vector2 v = simController.getSimulationRenderer().getCamViewport();
		
		super.init(id, simController, new Vector2(v.x/2, v.y/2));
		//getBody().setLinearVelocity(new Vector2(0,10));
	}

}
