package engine.entity;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import engine.behavior.TextureRenderer;
import game.constants.Textures;

public class RectArea extends Entity{

	float w,h;
	
	
	public RectArea(float w, float h) {
		super();
		this.w = w;
		this.h = h;
	}


	@Override
	protected void initFixture() {
		PolygonShape r = new PolygonShape();
		r.setAsBox(w/2, h/2);
		getBody().createFixture(r,1).setSensor(true);
		getBody().setType(BodyType.StaticBody);
	}

	@Override
	protected void initActions() {
		super.initActions();
		TextureRenderer re = new TextureRenderer();
		setRenderer(re);
		re.setTexture(Textures.GUILife100);

	}


	public Rectangle getRect() {
		Vector2 pos = getAbsolutePosition();
		return new Rectangle(pos.x-w,pos.y-h,w*2,h*2);
	}
	
}
