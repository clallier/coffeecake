package engine.entity;

import engine.behavior.tools.Bounce;



public class BouncingNotification extends Notification{

	public BouncingNotification(String s,int time) {
		super(s,time);
	}
	public BouncingNotification(String s) {
		super(s);
	}
	@Override
	protected void initMotion() {
		createBehavior(new Bounce());
	}
	
}
