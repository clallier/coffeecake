package engine.entity;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

import engine.SimulationController;
import engine.behavior.tools.Acceleration;
import engine.behavior.tools.NotifText;
import engine.behavior.tools.Temporary;


public class Notification extends Entity{
	private String text;
	private boolean relative;
	private NotifText textgui;
	private int time = 5000;

	public Notification(String s,boolean relative){
		this.text = s;
		this.relative = relative;
	}
	public Notification(String s){
		this.text = s;
		this.relative = true;
	}

	public Notification(String s, int time) {
		this(s);
		this.time = time;
	}
	@Override
	protected void initFixture() {
	}

	@Override
	public void init(int id, SimulationController simController, Vector2 pos) {
		super.init(id, simController, pos);
		getBody().setLinearVelocity(new Vector2(0,10));
	}

	@Override
	protected void initActions() {
		super.initActions();
		initMotion();
		textgui = (NotifText) createBehavior(new NotifText(text));
		textgui.relative = relative;
		Temporary t = (Temporary)createBehavior(new Temporary());
		t.setTimer(time);
	}
	protected void initMotion() {
		Acceleration a = (Acceleration) createBehavior(new Acceleration());
		a.setAngle(MathUtils.PI/2);
		a.setAcc(5);		
	}

	public void setText(String s) {
		textgui.text.setText(s);
	}

}
