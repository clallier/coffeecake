package engine;

import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.utils.ObjectMap;

import engine.input.AllKeyListener;
import engine.input.KeyInputProcessor;
import engine.input.KeyInputProcessor.Action;
import engine.input.PadInputAdapter;

public class InputController {
	public ObjectMap<InputType,KeyInputProcessor> inputs = new ObjectMap<InputType, KeyInputProcessor>();
	public ObjectMap<InputType,PadInputAdapter> joysticks = new ObjectMap<InputType, PadInputAdapter>();
	public AllKeyListener global;
	
	
	public enum InputType {Keyboard,Numpad_and_mouse, J1,J2,J3,J4, virtualPads};

	public InputController(AllKeyListener g){
		this.global = g;
	}
	
	public AllKeyListener getGlobal() {
		return global;
	}
	
	public void addInput(InputType t,KeyInputProcessor p){
		inputs.put(t, p);		
	}
	
	public void addInput(InputType joy, PadInputAdapter adapter) {
		joysticks.put(joy, adapter);
	}
	
	public void setEntityId(InputType t, ObjectMap<Action, Integer> binds, int id, boolean mouseRot) {
		if(t == InputType.Keyboard || t == InputType.Numpad_and_mouse){
			inputs.get(t).setId(id);
			inputs.get(t).mouseRot = mouseRot;
			inputs.get(t).setBind(binds);
		}else{
			if(joysticks.size != 0 ) {
				joysticks.get(t).setId(id);
				joysticks.get(t).setBind(binds);
			}
		}
	}

	public InputType getType(Controller c){
		for (InputType k : joysticks.keys()) {
			PadInputAdapter in = joysticks.get(k);
			if(in.getmController() == c){
				return k;
			}
		}
		return null;
	}

	public ObjectMap<Action, Integer> getBinds(int id) {
		for (KeyInputProcessor in : inputs.values()) {
			if(in.getmTargetEntityId() == id){
				return in.getBind();
			}
		}
		for(PadInputAdapter in : joysticks.values()){
			if(in.getmTargetEntityId() == id){
				return in.getBind();
			}
		}
		return null;
	}

	public InputType getInputType(int id) {
		for(InputType t : inputs.keys()){
			if(inputs.get(t).getmTargetEntityId() == id){
				return t;
			}
		}
		for(InputType t : joysticks.keys()){
			if(joysticks.get(t).getmTargetEntityId() == id){
				return t;
			}
		}
		return null;
	}
	
	/*
	public Values<PadInputAdapter> getListeners() {
		return joysticks.values();
	}
*/


}
