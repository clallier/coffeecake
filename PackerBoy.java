package engine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import com.badlogic.gdx.tools.imagepacker.TexturePacker2;
import com.badlogic.gdx.utils.XmlWriter;

import engine.defines.EngineTextures;
import engine.defines.EngineValues;

public class PackerBoy {
	
	// use TexturePacker in order to generate the texture pack
	// take a list of images and give a "game.atlas" texture atlas 
	// https://code.google.com/p/libgdx-texturepacker-gui/
	public static void packTo(String pathToAtlasAndroid) {
		String pathToTextures = "../"+EngineValues.texturesPath;
		String pathToAtlasDesktop = "bin/data/Atlas";
		
		TexturePacker2.process(pathToTextures, pathToAtlasAndroid, EngineTextures.atlasName);
		try {
			File sourceDir = new File(pathToAtlasAndroid);
			File destDir = new File(pathToAtlasDesktop); 
			destDir.mkdirs(); // make sure that the dest directory exists

			Path destPath = destDir.toPath();
			for (File sourceFile : sourceDir.listFiles()) {
				Path sourcePath = sourceFile.toPath();

				Files.copy(sourcePath, destPath.resolve(sourcePath.getFileName()));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}



		//TexturePacker2.process(pathToTextures, pathToAtlasDesktop, EngineTextures.atlasName);
	}
	
	@SuppressWarnings("unused")
	public static void jsonToXMl(String pathToAtlas) {

		String pathToXml = pathToAtlas.replace(".atlas", ".xml"); 
		
		BufferedReader reader;
		XmlWriter writer;
		try {
			
			//reader
			reader = new BufferedReader(new FileReader(pathToAtlas));
			String empty = reader.readLine();
			String name = reader.readLine();
			String format = reader.readLine();
			String filter = reader.readLine();
			String repeat = reader.readLine();
			
			// writer
			writer = new XmlWriter(new FileWriter(pathToXml));
			XmlWriter rootNode = writer.element("TextureAtlas").attribute("imagePath", name);
			XmlWriter subNode = null;
			
			int i=0, j=0;
			String currentLine;
			while ((currentLine = reader.readLine()) != null ) {
				++j;
				String textureName;
				
				// new attribute
				if(currentLine.startsWith("  ")) {
					String key = getKey(currentLine);
					
					// "  xy: 334, 45"
					if(key.compareTo("xy") == 0) {
						String[] content = getValues(currentLine);
						if(content.length > 0) {
							subNode.attribute("x", content[0]);
							subNode.attribute("y", content[1]);
						}
					}
					
					// "  xy: 334, 45"
					else if(key.compareTo("size") == 0) {
						String[] content = getValues(currentLine);
						if(content.length > 0) {
						subNode.attribute("width", content[0]);
						subNode.attribute("height", content[1]);
						}
					}
				} 
				
				// new texture name
				else {
					textureName = currentLine;
					if(name.isEmpty()) break;
					if(subNode != null) { subNode.pop(); }
					subNode = rootNode.element("SubTexture");
					subNode.attribute("name", textureName + ".png");
					++i;
				}
				

				
			}
			if(subNode != null) { subNode.pop(); }
			//System.out.println("written " +i+ " elements / " +j);
			writer.close();
			reader.close();
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static String[] getValues(String line) {
		line = line.replace(getKey(line) +":", "").trim(); // remove key
		String[] content = line.split(", "); // split values
		
		return content;
	}

	private static String getKey(String line) {
		String[] content = line.split(": ");
		if(content[0] != null)
			return content[0].trim();
		else return "";
	}
}
