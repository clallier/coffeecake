package engine.behavior;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import engine.SimulationController;
import engine.SimulationRenderer.Layer;
import engine.entity.Entity;

public abstract class LayoutBehavior extends Behavior {

	public Table layout = new Table();
	public Vector2 screenPos;
	public boolean relative = true; //Pos relative to camera

	@Override
	public void init(Entity entity, SimulationController simController) {
		super.init(entity, simController);
		initGUI();
		screenPos = new Vector2(layout.getX(),layout.getY());
		if(relative)
			mSimController.addGUIWidget(this);
		else
			mSimController.addActorToCurrentScene(layout, Layer.AbsoluteGUI);
	};


	@Override
	public void update(float dTime) {
		updateGUI(dTime);
	}

	@Override
	public void dispose() {
		super.dispose();
		if(layout != null) {
			layout.remove();
			layout.clear();
		}
	};

	protected abstract void initGUI();

	protected abstract void updateGUI(float dt);
}
