package engine.behavior.tools;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.utils.Align;

import engine.behavior.LayoutBehavior;
import engine.defines.EngineValues;

public class NotifText extends LayoutBehavior{
	public Label text;
	String s;
	
	public NotifText(String s){
		this.s = s;
		relative = false;
	}
	
	@Override
	protected void initGUI() {
		Skin skin = mSimController.getSimulationRenderer().getSkin();
		text = new Label(s, skin, EngineValues.font, Color.WHITE);
		text.setAlignment(Align.center);
		if(!relative){
			layout.setTransform(true);
			layout.setScale(mSimController.getSimulationRenderer().getScale());
		}
		layout.add(text).width(32).height(32).center();
	}

	@Override
	protected void updateGUI(float dt) {
		float scale = mEntity.getScaleFactor();
		Vector2 v = mEntity.getAbsolutePosition();
		layout.setPosition(v.x*scale,(v.y)*scale);
	}

}
