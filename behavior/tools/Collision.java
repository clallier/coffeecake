package engine.behavior.tools;

import engine.behavior.Behavior;
import engine.message.BaseMessage;
import engine.message.CollisionMessage;
import engine.message.MessageType;

public abstract class Collision extends Behavior{
	protected boolean askSensor = true;

	
	@Override
	public boolean handleMessage(BaseMessage msg) {
		if(msg.messageType == MessageType.BeginCollision){
			CollisionMessage c = (CollisionMessage) msg;
			if(!c.isSensor() || askSensor){
				onCollisionBegin(c.getOtherEntityId());
				return true;
			}
		}
		if(msg.messageType == MessageType.EndCollision){
			CollisionMessage c = (CollisionMessage) msg;
			if(!c.isSensor() || askSensor){
				onCollisionEnd(c.getOtherEntityId());
				return true;
			}
		}
		return false;
	}

	protected abstract void onCollisionBegin(int otherId);
	protected abstract void onCollisionEnd(int otherId);

	public void setAskSensor(boolean askSensor) {
		this.askSensor = askSensor;
	}

}
