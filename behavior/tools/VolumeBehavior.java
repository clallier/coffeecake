package engine.behavior.tools;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Array;

import engine.SoundPlayer;
import engine.behavior.LayoutBehavior;
import engine.defines.EngineSounds;
import engine.defines.EngineTextures;
import engine.defines.EngineValues;
import engine.message.PlaySound;


public class VolumeBehavior extends LayoutBehavior {
	TextButton minus;
	TextButton plus;
	Array<Image> volumeBar;
	int vol = 5;
	int max = 10;
	
	@Override
	protected void initGUI() {
		vol = (int) (SoundPlayer.volume*10);
		volumeBar = new Array<Image>();
		
		Skin skin = mSimController.getSimulationRenderer().getSkin();

		minus = new TextButton("-", skin, EngineValues.textButtonStyle);
		plus = new TextButton("+", skin, EngineValues.textButtonStyle);
		
		for(int i = 0;i < max;i++){
			Image bar = new Image(skin.getDrawable(EngineTextures.defaultBtnUp));
			volumeBar.add(bar);
		}
		int size = 18;
		layout.add(minus).center().width(size).height(size);
		for(int i = 0;i < max;i++){
			layout.add(volumeBar.get(i)).width(10).height((i+1)*3).align(Align.bottom);
		}
		layout.add(plus).width(size).height(size);
		plus.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				volumeUp();
			}
		});
		minus.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				volumeDown();
			}
		});
		layout.setPosition(EngineValues.screenw/2-100, -EngineValues.screenh/2+25);
	}

	protected void volumeUp() {
		if(vol < max)
			vol ++;
		mSimController.sendMessage(new PlaySound((1f*vol)/max));
		mSimController.sendMessage(new PlaySound(EngineSounds.button));
	}

	protected void volumeDown() {
		if(vol > 0)
			vol --;
		mSimController.sendMessage(new PlaySound((1f*vol)/max));
		mSimController.sendMessage(new PlaySound(EngineSounds.button));
	}

	@Override
	protected void updateGUI(float dt) {
		for(int i = 0; i < vol;i++){
			volumeBar.get(i).setVisible(true);
		}
		for(int i = vol;i < max ; i++){
			volumeBar.get(i).setVisible(false);
		}
	}

}
