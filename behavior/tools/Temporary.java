package engine.behavior.tools;

import engine.behavior.Behavior;

public class Temporary extends Behavior{

	/* Time in millis */
	protected float timer = 100;
	public Temporary() {
		super();
	}
	
	public Temporary(float ttl) {
		super();
		timer = ttl;
	}

	public float getTimer() {
		return timer;
	}
	
	public void setTimer(float timer) {
		this.timer = timer;
	}

	protected void onTimerEnd(){
		mEntity.destroy();
	}
	
	@Override
	public void update(float dTime) {
		timer -= dTime;
		if (timer <= 0)
			onTimerEnd();
	}
	
}
