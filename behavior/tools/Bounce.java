package engine.behavior.tools;

import com.badlogic.gdx.math.Vector2;

import engine.behavior.Behavior;

public class Bounce extends Behavior{
	Vector2 initpos = null;
	int bounceSpeed = 10; 
	int decForce = 25;
	
	@Override
	public void update(float dTime) {
		Vector2 p = mEntity.getRelativePosition();
		if(initpos == null)
			initpos = mEntity.getRelativePosition().cpy().add(0,mEntity.getSize().y);
		if(p.y < initpos.y){
			mEntity.getBody().setLinearVelocity(new Vector2(0,0));
			mEntity.getBody().applyLinearImpulse(new Vector2(0,bounceSpeed*mEntity.getBody().getMass()), mEntity.getBody().getLocalCenter(), true);
		}else{
			mEntity.getBody().applyForceToCenter(new Vector2(0,-decForce*mEntity.getBody().getMass()), true);
		}
	}

	
	
}
