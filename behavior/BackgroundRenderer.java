package engine.behavior;

import engine.SimulationController;
import engine.SimulationRenderer.Layer;
import engine.defines.EngineValues;
import engine.tileMap.ArrayGrid;
import game.constants.Constants.Block;
import game.constants.Constants.Environment;
import game.constants.Textures;

import java.util.Iterator;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;

public class BackgroundRenderer extends ImageBehavior {

	ArrayMap<Drawable, Float> tileProbas; //Image, proba
	Array<Drawable> ground;
	Group bgImage;
	Vector2 anchor;
			
	@Override
	protected void initImage() {
	}

	@Override
	protected void updateImage(float dt) {
	}
	
	@Override
	public void setColor(Color color) {
	}

	@Override
	public Color getColor() {
		return null;
	}

	@Override
	public void dispose() {
		bgImage.remove();
		ground.clear();
	}

	public void setMap(ArrayGrid<Integer> currentMap, Environment environment) {
		String textureName = Textures.groundPrefix + environment.name();
		
		tileProbas = new ArrayMap<Drawable, Float>();
		tileProbas.put(mSimRenderer.getSkin().getDrawable(textureName + "0"), 0.85f); //85 % to spawn
		tileProbas.put(mSimRenderer.getSkin().getDrawable(textureName + "1"), 0.92f); //7 % to spawn
		tileProbas.put(mSimRenderer.getSkin().getDrawable(textureName + "2"), 0.99f); //7 % to spawn
		tileProbas.put(mSimRenderer.getSkin().getDrawable(textureName + "3"), 1.0f); //1 % to spawn
		ground = new Array<Drawable>();
		anchor = mSimController.getActiveScene().getPos();
		
		generateTiles(currentMap);
		addTilesToScene(currentMap);
	}

	private void addTilesToScene(ArrayGrid<Integer> currentMap) {
		int s = (int) EngineValues.bodyScale;
		bgImage = new Group();
		Iterator<Drawable> it = ground.iterator();
		
		
		for(int i=0 ; i<currentMap.getWidth(); ++i ) {
			for(int j=0 ; j<currentMap.getHeight(); ++j ) {
				int k = currentMap.getHeight()-1-j;
				if(it.hasNext()) {
					Drawable d = it.next();
					if(d != null) {
						Image img = new Image(d);
						img.setOrigin(img.getWidth()/2,img.getHeight()/2);
						img.setX(anchor.x*s + i* SimulationController.TILE_SIZE *s);
						img.setY(anchor.y*s + k* SimulationController.TILE_SIZE *s);
						bgImage.addActor(img);
					}
				}
			}
		}
		
		bgImage.setScale(mScale.x,mScale.y);
		bgImage.setOrigin(bgImage.getWidth()/2,bgImage.getHeight()/2);
		mSimController.addActorToCurrentScene(bgImage, Layer.Background);
	}

	private void generateTiles(ArrayGrid<Integer> currentMap) {
		if(ground != null) {
			for(int i=0 ; i<currentMap.getWidth(); ++i ) {
				for(int j=0 ; j<currentMap.getHeight(); ++j ) {
					int k = currentMap.getHeight()-1-j;
					Drawable d = null;
					
					if( /*currentMap.get(k, i) != Block.Obstacle.ordinal() ||*/ currentMap.get(k, i) != Block.Wall.ordinal()) {
						if(currentMap.get(k, i) == Block.DropZone.ordinal())
							d = mSimRenderer.getSkin().getDrawable(Textures.dropzone);
						else
							d = tileRoulette();
					}
					ground.add(d);
				}
				//System.out.println(" ");
			}
		}
	}

	private Drawable tileRoulette() {
		float r = MathUtils.random.nextFloat();
		Drawable d = null;
		
		// super random heavy roulette 
		for(int p=0;p<tileProbas.size;++p) {
			if(r<tileProbas.getValueAt(p)) {
				d = tileProbas.getKeyAt(p);
				//System.out.print(p + " ");
				break;
			}
		}
		return d;
	}

	@Override
	public void setLayer(Layer debug) {		
	}

}
