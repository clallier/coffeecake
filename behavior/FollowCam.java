package engine.behavior;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

import engine.SimulationController;
import engine.defines.EngineValues;

public class FollowCam extends Behavior{

	float left;
	float right;
	float worldH;
	private float maxSpeed = 150;
	private float maxLength = 500;
	private float epsilon = 5;

	private Vector2 offset = new Vector2();
	private Vector2 kick = new Vector2(), viewOffset = new Vector2();
	private float offsetSize = 30;

	public FollowCam(float maxW,float maxH){
		offset.x += SimulationController.TILE_SIZE*EngineValues.bodyScale;
	}

	@Override
	public void update(float dTime) {
		float w = EngineValues.viewportW;
		float h = EngineValues.viewportH;
		Vector2 pos = mEntity.getAbsolutePosition().cpy();
		pos.scl(mEntity.getScaleFactor());
		Vector2 o = offset;

		float a = mEntity.getAngle();
		float cos = MathUtils.cos(a);
		float sin = MathUtils.sin(a)/2;
		viewOffset.x = cos*offsetSize ;
		viewOffset.y = sin*offsetSize;
		pos = pos.add(viewOffset);

		if(kick.len() > 0.1){
			kick.scl(0.9f);
		}else{
			kick.x = 0;
			kick.y = 0;
		}
		if(right-left > w){
			if(pos.x+w/2 > right){
				pos.x = right-w/2;
			}
			if(pos.x-w/2 < left){
				pos.x = left+w/2;
			}
		}else{
			pos.x = o.x+(right-left)/2;
		}
		
		if(worldH > h){
			if(pos.y+h/2 > o.y+worldH){
				pos.y = o.y+worldH-h/2;
			}
			if(pos.y-h/2 < o.y){
				pos.y = o.y+h/2;	
			}
		}else{
			pos.y = o.y+h/2;
		}

		Vector2 tar = pos.add(kick);
		Vector3 cam = mSimController.getSimulationRenderer().getCamPos();
		Vector2 diff = new Vector2(tar.x-cam.x,tar.y-cam.y);
		if(!diff.epsilonEquals(0, 0, epsilon)){
			Vector2 dir = diff.nor();
			Vector2 speed = new Vector2(maxSpeed *diff.x*diff.x/maxLength ,maxSpeed*diff.y*diff.y/maxLength);
			cam.add(dir.x*speed.x*dTime, dir.y*speed.y*dTime, 0);
		}else{
			cam.set(tar.x, tar.y, 0);
		}
		mSimController.getSimulationRenderer().setCamPos(cam.x,cam.y);
	}

	public void kick(Vector2 kick){
		this.kick  = kick.cpy().scl(mEntity.getScaleFactor()*0.25f);
	}

//	public void startTransition(Vector2 pos,Runnable callback){
//		this.offset = pos.cpy().scl(mEntity.getScaleFactor());
//		offset.x += SimulationController.TILE_SIZE*EngineValues.bodyScale;
//		this.callback = callback;
//	}

	public void updateWorldSize() {
		right = (mSimController.getActiveScene().getPos().x+mSimController.WORLD_W)*EngineValues.bodyScale;
		worldH = (mSimController.WORLD_H-1) * EngineValues.bodyScale;
	}
}


