package engine.behavior;

import java.util.HashMap;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap.Entry;

import engine.SimulationController;
import engine.SimulationRenderer.Layer;
import engine.defines.EngineValues;
import engine.entity.Entity;
import engine.entity.Entity.Orientation;
import engine.render.OrientedAnim;
import game.entity.mobs.Nored;

public class OrientedTextureRenderer extends ImageBehavior {

	public OrientedAnim mOriented;
	public HashMap<String, Image> mImages;
	public HashMap<String, TextureRegionDrawable> currentSubTextures;
	Label t;
	protected Vector2 suboffset = new Vector2();
	
	Vector2 pos = new Vector2();
	Vector2 src;

	public float animTimer = 0;
	public int animCounter = 0;
	public float ratio = 1;

	private Layer mLayer = Layer.Actors;
	private Color mColor = null;

	private boolean colorChanged;

	public OrientedTextureRenderer() {
		mImages = new HashMap<String, Image>();
		currentSubTextures = new HashMap<String, TextureRegionDrawable>();
	}

	@Override
	public void init(Entity entity, SimulationController simController) {
		super.init(entity, simController);
		src = mEntity.getAbsolutePosition();
	}

	public void clearMods(){
		Array<String> torm = new Array<String>();
		for (String k : mImages.keySet()) {
			if(!mOriented.getMods().contains(k, false)){
				if(!k.equals("main")){
					Image i = mImages.get(k);
					i.remove();
					torm.add(k);
				}
			}
		}
		for (String string : torm) {
			if(!string.equals("main")){
				mImages.remove(string);
				currentSubTextures.remove(string);
			}
		}
	}

	public void setTexture(OrientedAnim t){
		mOriented = t;
		animTimer = 0;
		clearMods();
	}

	public OrientedAnim getmOriented() {
		return mOriented;
	}

	@Override
	protected void updateImage(float dt) {
		if(mOriented == null)
			return;
		//if(mEntity instanceof Nored)
		//	System.out.println();
		animTimer += dt*ratio;
		Orientation o = mEntity.getOrient();
		animCounter = mOriented.getAnim(o).getDisplayCount(animTimer);
		Image i = setSubTexture("main",(TextureRegionDrawable)mOriented.getAnim(o).getTexture(animTimer), new Vector2(), this.mLayer, new Vector2());
		
		if(EngineValues.zindexDebug) {
			if(t != null)t.remove();
			t= new Label(""+i.getZIndex(), mSimRenderer.getSkin(), EngineValues.labelStyle);
			mSimController.addActorToCurrentScene(t, this.mLayer);
			t.setWrap(false);
			t.setX(i.getX());
			t.setY(i.getY());
			t.setZIndex(Integer.MAX_VALUE);
		}
		
		for (Entry<String, OrientedAnim> e : mOriented.getChildTextures().entries()) {
			setSubTexture(e.key,(TextureRegionDrawable)e.value.getAnim(o).getTexture(animTimer),
					mOriented.getHotPoint(e.key).getPoint(o),
					mOriented.getHotPoint(e.key).getLayer(),
					e.value.getSelf().getPoint(o));
		}

	}

	private Image setSubTexture(String key,TextureRegionDrawable r, Vector2 off, Layer layer, Vector2 selfJunction) {
		/*Texture is empty, clean image if necessary*/
		if(r == null){
			clearSubTexture(key);
			return null;
		}
		/*Create image for subtexture if it's not cached*/
		if(!mImages.containsKey(key) && r != null){
			Image i = new Image();
			mImages.put(key, i);
			i.setName("test oriented " + mEntity.getClass().getSimpleName() + " "+mEntity.getId());
			mSimController.addActorToCurrentScene(i,layer);
		}

		Image i = mImages.get(key);

		if(currentSubTextures.get(key) != r || colorChanged){
			colorChanged = false;
			currentSubTextures.put(key,r);
			i.setDrawable(r);
			i.setScale(mScale.x, mScale.y);

			if(mColor != null)
				i.setColor(mColor);

			if(r!=null)
				i.setSize(r.getRegion().getRegionWidth(),r.getRegion().getRegionHeight());


			if(key == "main"){ //initialise offset to center
				suboffset.x = i.getWidth()*mScale.x/2; 
				suboffset.y = i.getHeight()*mScale.y/2;
			}
		}
		/*Position of body in pixel*/
		pos = mEntity.getAbsolutePosition().add(offset);
		pos.scl(mEntity.getScaleFactor());
		/*Substract offset to get the position of the left corner of the main texture */
		pos.sub(suboffset.x,suboffset.y);
		/*add subtexture offset to the hotpoint*/
		pos.add(off.cpy().scl(mScale));
		/*add subtexture self offset*/
		pos.sub(selfJunction.cpy().scl(mScale));
		i.setPosition(pos.x, pos.y);
		//int z = mEntity.getLocalZIndex()/*/EngineValues.MAX_Z*EngineValues.RANGE*/;
		//i.setZIndex( z/*+ layer.ordinal()*EngineValues.RANGE*/ );
	
		return i;
	}

	private void clearSubTexture(String key) {
		Image i = mImages.get(key);
		if(i != null){
			i.remove();
			mImages.remove(key);
			currentSubTextures.remove(key);
		}
	}

	@Override
	protected void initImage() {

	}

	@Override
	public void setColor(Color color) {
		mColor = color;
		colorChanged = true;
	}

	@Override
	public Color getColor() {
		return mImages.get("main").getColor();
	}

	@Override
	public void dispose() {
		for (Image i : mImages.values()) {
			i.remove();
		}
		
		if(t != null)t.remove();
	}

	public Layer getLayer() {
		return mLayer;
	}

	public void setLayer(Layer layer) {
		this.mLayer = layer;
	}
}

