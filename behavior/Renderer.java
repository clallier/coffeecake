package engine.behavior;

import com.badlogic.gdx.graphics.Color;

import engine.SimulationController;
import engine.SimulationRenderer;
import engine.entity.Entity;

/**
 * @Suppr
 * @author godjam
 *
 */
@Deprecated
public abstract class Renderer extends Behavior {
	protected boolean drawSensor = false;
	protected Color mColor;
	
	protected SimulationRenderer mSimRenderer;
	
	public void setDrawSensor(boolean drawSensor) {
		this.drawSensor = drawSensor;
	}
	
	public Renderer() {
		mColor = Color.WHITE;
		
	}
	
	@Override
	public void init(Entity entity, SimulationController simController) {
		super.init(entity,simController);
		mSimRenderer = mSimController.getSimulationRenderer();
	}
	
	@Override
	public void update(float dTime) {
	}
	
	public abstract void render();

	public Color getColor() {
		return mColor;
	}

	public void setColor(Color mColor) {
		this.mColor = mColor;
	}

}
