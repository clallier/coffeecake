package engine.behavior;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad.TouchpadStyle;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

import engine.defines.EngineTextures;
import engine.defines.EngineValues;
import engine.entity.Hero;
import engine.message.InputMessage;

public class TouchpadGUI extends LayoutBehavior{
	Hero hero;
	boolean A;
	private Touchpad padLeft, padRight;
	Vector2 dir, orient;
	
	//private TextButton b;
	public TouchpadGUI(Hero h){
		this.hero = h;
		dir = new Vector2();
		orient = new Vector2();
	}
	
	/*
	@Override
	public boolean handleMessage(BaseMessage msg) {
		float w = EngineValues.screenw/2, h = EngineValues.screenh/2;

		if(msg instanceof TouchPadMoveMsg){
			TouchPadMoveMsg m = (TouchPadMoveMsg) msg;
			
			Vector2 p = new Vector2(m.x, m.y);
			p.x -= w;
			p.y = h-p.y;
			p.x -= layout.getWidth()/2;
			p.y -= layout.getHeight()/2;
			layout.setPosition(p.x, p.y);	
		}
		return super.handleMessage(msg);
	}*/

	@Override
	protected void initGUI() {

		float w = EngineValues.screenw, h = EngineValues.screenh;
		
		Skin skin = mSimController.getSimulationRenderer().getSkin();

		NinePatchDrawable dL = (NinePatchDrawable) skin.getDrawable(EngineTextures.knobL);
		NinePatchDrawable kL = (NinePatchDrawable) skin.getDrawable(EngineTextures.knobL);
		NinePatchDrawable dR = (NinePatchDrawable) skin.getDrawable(EngineTextures.knobL);
		NinePatchDrawable kR = (NinePatchDrawable) skin.getDrawable(EngineTextures.knobR);
	
		padLeft = new Touchpad(0.1f, new TouchpadStyle(dL,kL));
		padRight = new Touchpad(0.1f, new TouchpadStyle(dR,kR));

		int interpad = (int) (w - h/2);
		layout.add(padLeft).size(h/4);
		layout.add(padRight).size(h/4).padLeft(interpad);
	    layout.setPosition(0, -3*h/8);
	    


	}

	@Override
	protected void updateGUI(float dt) {
		Vector2 oldDir = dir;
		Vector2 oldOrient = orient;
				
		dir = updatePadLeft();
		orient = updatePadRight();
		
		if(dir.epsilonEquals(oldDir, 0.05f) == false || orient.epsilonEquals(oldOrient, 0.05f) == false)
			mSimController.sendMessage(new InputMessage(hero.getId(), dir, orient, A, false));
	}

	private Vector2 updatePadLeft() {
		float x = padLeft.getKnobPercentX();
		float y = padLeft.getKnobPercentY();
		Vector2 d = new Vector2(x,y);
		return d;
	}

	private Vector2 updatePadRight() {
		float x = padRight.getKnobPercentX();
		float y = padRight.getKnobPercentY();
		Vector2 d = new Vector2(x,y);

		//shoot threshold
		if(d.len() > 0.9f){
			A = true;
		} else A = false;
		
		return d;
	}
}
