package engine.behavior;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

import engine.SimulationRenderer.Layer;
import engine.defines.EngineValues;
import engine.render.NormalImage;

public class TextureRenderer extends ImageBehavior {
	protected Image image= new NormalImage();
	private Layer layer = Layer.Actors;
	Label t;
	
	protected void initImage() {
		//setTexture(Textures.def);
	}
	
	protected void updateImage(float dt) {
		image.setScale(mScale.x,mScale.y);
		image.setRotation(mEntity.getAngle()*MathUtils.radDeg);
		Vector2 p = mEntity.getAbsolutePosition().add(this.offset);
		p.scl(mEntity.getScaleFactor());
		float a = mEntity.getBody().getAngle();
		image.setRotation(a*MathUtils.radDeg);
		float w = image.getWidth()/2;
		float h = image.getHeight()/2;
		image.setPosition(p.x-w, p.y-h);
		
		if(EngineValues.zindexDebug) {
			if(t != null)t.remove();
			t= new Label(""+image.getZIndex(), mSimRenderer.getSkin(), EngineValues.labelStyle);
			mSimController.addActorToCurrentScene(t, layer);
			t.setWrap(false);
			t.setX(image.getX());
			t.setY(image.getY());
		}
	}
	
	@Override
	public void dispose() {
		if(image != null) {
			image.remove();
		}
		if(t != null)t.remove();
	};
	
	public void setColor(Color color) {
		image.setColor(color);
	}
	
	public Color getColor() {
		return image.getColor();
	}
	
	public void setTexture(String textureName){
		Skin skin = mSimRenderer.getSkin();

		if(image != null)
			image.remove();
		
		image = new NormalImage(skin.getDrawable(textureName),null);
		image.setScale(mScale.x,mScale.y);
		image.setOrigin(image.getWidth()/2,image.getHeight()/2);
		image.setName(mEntity.getClass().getSimpleName());
		mSimController.addActorToCurrentScene(image, layer);
	}

	
	
	public Layer getLayer() {
		return layer;
	}  

	public void setLayer(Layer layer) {
		this.layer = layer;
		image.remove();
		image.setName("test image");	
		mSimController.addActorToCurrentScene(image, layer);
	}

	public void setSize(float w, float h) {
		float x = (w*EngineValues.bodyScale)/image.getWidth();
		float y = (h*EngineValues.bodyScale)/image.getHeight();
		setmScale(x,y);
	}

	/*public void setTexture(Texture t) {
		if(image != null)
			image.remove();
		
		image = new Image(t);
		image.setScale(mScale.x,mScale.y);
		image.setOrigin(image.getWidth()/2,image.getHeight()/2);
		mSimController.addActorToCurrentScene(image, layer);
		System.out.println("settexture " + layer);

	}*/
}

