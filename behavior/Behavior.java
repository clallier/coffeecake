package engine.behavior;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;

import engine.SimulationController;
import engine.entity.Entity;
import engine.message.BaseMessage;


public abstract class Behavior implements Disposable{
	protected Entity mEntity;
	protected SimulationController mSimController;
	protected Array<Entity> mTargets;
	private int id;
	public boolean enabled = true; 

	public boolean isDestroyed(){
		return id == -1;
	}
	
	protected Behavior() {
		mTargets = new Array<Entity>();
	}
	
	public void init(Entity entity, SimulationController simController) {
		mEntity = entity;
		mSimController = simController;
	};
	
	public void checkAvailiability() {};
	public boolean handleMessage(BaseMessage msg){return false;};
	public abstract void update(float dTime);

	public Array<Entity> getTargetList() {
		return mTargets;
	}

	public void addTarget(Entity target) {
		mTargets.add(target);
	}
	
	@Override
	public void dispose() {
	}

	public void setId(int bid) {
		this.id = bid;
		
	}

	public int getId() {
		return id;
	}
	
}
