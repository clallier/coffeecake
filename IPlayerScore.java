package engine;

import com.badlogic.gdx.utils.JsonValue;

public interface IPlayerScore {
	
	public void read(JsonValue current);
	public int getScore();
	public String getName();
	public int getTime();
	public int getLvl();
	public int getMaxComboTime();
}
