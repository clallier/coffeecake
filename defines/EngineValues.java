package engine.defines;

public class EngineValues {
		public enum lang{en,fr};
		public static lang language = lang.en;
		
		public static final float bodyScale = 10;
		public static final float GUIIconSize = 32;

		/*Graphics*/
		public static int screenw = 800;
		public static int screenh = 700;
		public static float viewportRatio = 4f; //Ratio for android
		public static float viewportW = 400f;
		public static float viewportH = 400f;

		public static final String font = "data/font/default.fnt";
		public static final String classicFont = "data/font/classic.fnt";
		public static final String labelStyle = "labelStyle";
		public static final String classicLabelStyle = "classicLabelStyle";
		public static final String textButtonStyle = "textButtonStyle";
		public static final String classicTextButtonStyle = "classicTextButtonStyle";

		public static final int RANGE = 500;
		public static final int MAX_Z = 500;


		//Should be false	
		public static boolean box2DDebug = false;
		public static boolean awesomeLight = false;
		public static boolean KongregateAPI = false;
		public static boolean mobileDebug = true;
		public static boolean adDebug = false;
		public static final boolean fullScreenDebug = true;
		public static final boolean texturePackingDebug = false;
		public static final boolean controlDebug = true;
		public static final boolean zindexDebug = false;
		
		public static final int menuH = 80;

		public static final String keyboardText = "Press [SPACE] to use keyboard";

		public static final String numpadText = "Press [0] to use num pad";

		public static final String texturesPath = "texture";
}
