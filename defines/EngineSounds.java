package engine.defines;

public class EngineSounds {
	public static final String soundDir = "data/snd/";
	public static final String musicDir = "data/snd/";
	
	public static final String button = "button.ogg";
	public static final String newPlayer = "newPlayer.ogg";

	public static String[] sounds = {button, newPlayer};
}
