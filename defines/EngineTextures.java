package engine.defines;

public class EngineTextures {
	public final static String atlasName 	= "game";
	public static String defaultBtnUp 		= "valid";
	public static String defaultBtnDown 	= "button";
	public static String knobL 				= "transparent/knobL";
	public static String knobR 				= "transparent/knobR";
}
